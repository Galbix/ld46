﻿using UnityEngine;

[System.Serializable]
public class Fade : TweenCustom
{
    public Color fadeColor = Color.black;
    public float preDelayTime;
    public float postDelayTime;
}
