﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class SceneController
{
    public enum SceneName
    {
        NONE,
        MAIN,
        OVER,

        Length
    }
    
    private static int GetSceneNameLength()
    {
        return (int)SceneName.Length;
    }
    
    private static string[] sceneNameStr = new string[]
    {
        string.Empty,
        "_Main",
        "GameOver",
    };
    
    public static string GetSceneNameStr(this SceneName name)
    {
        return sceneNameStr[(int)name];
    }
    
    private static string GetActiveSceneNameStr()
    {
        return SceneManager.GetActiveScene().name;
    }
    
    private static int GetActiveSceneIndex()
    {
        return Array.IndexOf(sceneNameStr, GetActiveSceneNameStr());
    }
    
    public static bool HaveNextScene()
    {
        return GetActiveSceneIndex() + 1 < GetSceneNameLength();
    }
    
    public static void LoadNext()
    {
        int activeSceneIndex = GetActiveSceneIndex();
        
        if(activeSceneIndex > -1)
        {
            SceneName nextSceneName = (SceneName)activeSceneIndex + 1;
            Load(nextSceneName);
        }
        else
        {
            Debug.Log("ERROR! Active Scene Name not found!!");
        }
    }
    
    public static void Load(SceneName sceneName)
    {
        Load(sceneName.GetSceneNameStr());
    }
    
    public static void Load(string sceneNameStr)
    {
        SceneManager.LoadSceneAsync(sceneNameStr);
    }
    
    public static void Restart()
    {
        Load(GetActiveSceneNameStr());
    }
}
