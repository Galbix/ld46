﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MEC;

public class Bezier : MonoBehaviour
{
    [Header("Bezier Curve Parameters")]
    public Transform startPoint, controlPoint, endPoint;
    public float bezierTime;

    [Header("Optionals Parameters")]
    public AnimationCurve bezierCurve;
    public bool cyclic;
    
    private void Start()
    {
        IEnumerator<float> bezierCoroutine = cyclic ? ApplyCyclicBezierCurve() : ApplyBezierCurve();
        Timing.RunCoroutine(bezierCoroutine);
    }

    private IEnumerator<float> ApplyBezierCurve()
    {
        IEnumerator<float> movementBezier = BezierUtil.MovementBezier(transform, startPoint.position, controlPoint.position,
            endPoint.position, bezierTime, bezierCurve);
            
        yield return Timing.WaitUntilDone(Timing.RunCoroutine(movementBezier));
    }

    private IEnumerator<float> ApplyCyclicBezierCurve()
    {
        IEnumerator<float> returnMovementBezier = BezierUtil.MovementBezier(transform, startPoint.position, controlPoint.position, 
            endPoint.position, bezierTime, bezierCurve);
            
        yield return Timing.WaitUntilDone(Timing.RunCoroutine(returnMovementBezier));
        Timing.RunCoroutine(ApplyCyclicBezierCurve());
    }
}
