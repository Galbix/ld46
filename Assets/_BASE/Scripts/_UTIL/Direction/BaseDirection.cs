﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class BaseDirection : MonoBehaviour
{
    public Transform targetTransform;
    public Directions.Direction initialDirection;
    public ChangeMode changeDirectionMode = ChangeMode.SCALE;
    //public bool reverseDirectionValues;
    
    public enum ChangeMode
    {
        NONE,
        ROTATION,
        SCALE
    }
    
    [ShowIf("IsRotationEffectActive")]
    [Range(0, 1)][Title("Rotation Effect")] 
    public float directionSmoothing;
    
    // Direction
    protected Directions.Direction currentDirection = Directions.Direction.NONE;
    
    // Rotation Mode
    protected Vector3 targetDirectionRotation;
    protected Vector3 currentVelocityDirectionRotation;   // ref
    
    private float LEFT_SCALE_VALUE = 0f;
    private float RIGHT_SCALE_VALUE = 0f;
    
    private float LEFT_ROT_VALUE = 0f;
    private float RIGHT_ROT_VALUE = 0f;
    
    #region UNITY METHODS
    private void Awake() 
    {
        CheckSetup();
    }
    
    private void FixedUpdate() 
    {        
        //CheckDirection();
    }
    #endregion
    
    private bool isSetupReady = false;
    
    private void CheckSetup()
    {
        if(!isSetupReady)
        {
            SetupDirections();
        }
    }
    
    private void SetupDirections()
    {
        if(initialDirection.Equals(Directions.Direction.LEFT))
        {
            LEFT_SCALE_VALUE = GetTargetTransform().localScale.x;
            RIGHT_SCALE_VALUE = LEFT_SCALE_VALUE * -1f;
            
            LEFT_ROT_VALUE = GetTargetTransform().localRotation.eulerAngles.y;
            RIGHT_SCALE_VALUE = LEFT_ROT_VALUE > 0f ? 0f : 180f;
        }
        
        if(initialDirection.Equals(Directions.Direction.RIGHT))
        {
            RIGHT_SCALE_VALUE = GetTargetTransform().localScale.x;
            LEFT_SCALE_VALUE = RIGHT_SCALE_VALUE * -1f;
            
            RIGHT_SCALE_VALUE = GetTargetTransform().localRotation.eulerAngles.y;
            LEFT_ROT_VALUE = RIGHT_SCALE_VALUE > 0f ? 0f : 180f;
        }
        
        isSetupReady = true;
    }
    
    public virtual void Init(Directions.Direction _newInitialDirection)
    {
        CheckSetup();        
        ChangeDirection(_newInitialDirection);
    }
    
    public virtual void Init()
    {
        Init(initialDirection);
    }
    
    protected virtual void CheckDirection()
    {
        Directions.Direction targetDirection = GetCurrentDirection();
        
        if(ShouldChangeDirection(targetDirection))
        {
            ChangeDirection(targetDirection);
        }
        
        if(ShouldUpdateDirectionRotation())
        {
            GetTargetTransform().rotation = Quaternion.Euler(Vector3.SmoothDamp(GetTargetTransform().rotation.eulerAngles,
              targetDirectionRotation, ref currentVelocityDirectionRotation, directionSmoothing));
        }
    }
    
    protected virtual bool ShouldChangeDirection(Directions.Direction _targetDirection)
    {
        return !currentDirection.Equals(_targetDirection);
    }
    
    [Button]
    public void ChangeDirection()
    {
        ChangeDirection(currentDirection.GetOppositeDirection());
    }
    
    protected virtual void ChangeDirection(Directions.Direction newDirection)
    {
        if(currentDirection.Equals(newDirection)) return;
        
        currentDirection = newDirection;
        
        if(IsRotationEffectActive())
        {
            GetTargetTransform().rotation = Quaternion.Euler(GetDirectionRotation(currentDirection));
        }
        
        if(IsScaleEffectActive())
        {
            GetTargetTransform().localScale = GetTargetTransform().localScale.WithX(GetCustomDirectionValue());
        } 
        
        OnChangeDirection();
    }
    
    protected virtual void OnChangeDirection() {}
    
    protected Vector3 GetDirectionRotation(Directions.Direction direction)
    {
        switch(direction)
        {
            case Directions.Direction.RIGHT:
            default:
                return new Vector3(0f, RIGHT_ROT_VALUE, 0f);
                
            case Directions.Direction.LEFT:
                return new Vector3(0f, LEFT_ROT_VALUE, 0f);
        }
    }
    
    protected bool ShouldUpdateDirectionRotation()
    {
        if(!IsRotationEffectActive()) return false;
        
        return GetTargetTransform().rotation.eulerAngles != targetDirectionRotation;
    }
    
    protected bool IsRotationEffectActive()
    {
        return changeDirectionMode.Equals(ChangeMode.ROTATION);
    }
    
    protected bool IsScaleEffectActive()
    {
        return changeDirectionMode.Equals(ChangeMode.SCALE);
    }
    
    public virtual Directions.Direction GetCurrentDirection()
    {
        if(currentDirection.Equals(Directions.Direction.NONE))
        {
            return initialDirection;
        }
        
        return currentDirection;
    }
    
    protected float GetCustomDirectionValue()
    {
        return currentDirection.Equals(Directions.Direction.LEFT) ? LEFT_SCALE_VALUE : RIGHT_SCALE_VALUE;
    }
    
    private Transform GetTargetTransform()
    {
        return targetTransform ? targetTransform : this.transform;
    }
}
