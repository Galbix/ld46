﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Directions
{
	public enum Direction
	{
		NONE,
		UP,
		RIGHT,
		DOWN,
		LEFT
	}
	
	private static float[] FLOAT_VALUE = new float[]
	{
		0f,
		1f,
		1f,
		-1f,
		-1f
	};
	
    public static int[] INT_VALUE = new int[]
    {
        0,
        1,
        1,
        -1,
        -1
    };
	
	private static Vector2[] VECTOR_VALUE = new Vector2[]
	{
		Vector2.zero,
		Vector2.up,
		Vector2.right,
		Vector2.down,
		Vector2.left,
	};
	
	private static Direction[] VECTOR_OPPOSITE = new Direction[]
	{
		Direction.NONE,
		Direction.DOWN,
		Direction.LEFT,
		Direction.UP,
		Direction.RIGHT
	};
	
	private static List<Direction> HORIZONTAL_DIRECTIONS = new List<Direction>
	{
		Direction.LEFT,
		Direction.RIGHT
	};
	
	private static List<Direction> VERTICAL_DIRECTIONS = new List<Direction>
	{
		Direction.UP,
		Direction.DOWN
	};

    public static float GetDirectionFloatValue(this Direction direction)
    {
        return FLOAT_VALUE[(int)direction];
    }
    
    public static int GetDirectionIntValue(this Direction direction)
    {
        return INT_VALUE[(int)direction];
    }

    public static Vector2 GetDirectionVector(this Direction direction)
    {
        return VECTOR_VALUE[(int)direction];
    }
    
    public static Direction GetOppositeDirection(this Direction direction)
    {
        return VECTOR_OPPOSITE[(int)direction];
    }
    
    public static Direction GetRandomHorizontalDirection()
    {
        return Random.value > .5f ? Direction.LEFT : Direction.RIGHT;
    }

    public static Direction GetRandomVerticalDirection()
    {
        return Random.value > .5f ? Direction.UP : Direction.DOWN;
    }
}
