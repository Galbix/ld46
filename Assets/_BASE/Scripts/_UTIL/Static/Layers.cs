﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Layers
{
    private static string UI = "UI";
    
    /// LAYERS (int)
    public static int GetUILayer()
    {
        return LayerMask.NameToLayer(UI);
    }
    
    /// LAYERMASKS (LayerMask)
    public static LayerMask GetUILayerMask()
    {
        return LayerMask.GetMask(UI);
    }
}
