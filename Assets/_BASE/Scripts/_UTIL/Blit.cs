﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Blit : MonoBehaviour {

    public Material transitionMaterial;

    private void OnRenderImage(RenderTexture src, RenderTexture dst)
    {
        if (transitionMaterial != null)
        {
            Graphics.Blit(src, dst, transitionMaterial);
        }
    }
}
