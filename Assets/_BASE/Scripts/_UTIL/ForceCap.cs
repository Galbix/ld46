﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Enables targeting specific framerates (for testing)
/// </summary>
public class ForceCap : MonoBehaviour
{
    public bool activeCap;
    public int targetFPS;

    private void Start()
    {
        ForcedFPS();
    }

    private void ForcedFPS()
    {
        if (!activeCap) return;

        Application.targetFrameRate = targetFPS;
        QualitySettings.vSyncCount = 0;
    }
}
