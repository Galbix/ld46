﻿using UnityEngine;
using System.Collections;

public static class ColorUtil
{
    public static Color whiteNoAlpha = new Color(1f, 1f, 1f, 0f);
    public static Color blackNoAlpha = new Color(0f, 0f, 0f, 0f);

    public static Color GetColorNoAlpha(Color targetColor)
    {
        return new Color(targetColor.r, targetColor.g, targetColor.b, 0f);
    }
}
