﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class VectorUtil
{
    public enum AxisType
    {
        ZERO,
        ONE,
        UP,
        DOWN,
        LEFT,
        RIGHT,
        FORWARD,
        BACK
    }
    
    public enum Random
    {
        HORIZONTAL,
        VERTICAL
    }
    
    private static Vector3[] axisVector = new Vector3[]
    {
        Vector3.zero,
        Vector3.one,
        Vector3.up,
        Vector3.down,
        Vector3.left,
        Vector3.right,
        Vector3.forward,
        Vector3.back
    };
    
    public static Vector3 GetAxisVector(this AxisType axisType)
    {
        return axisVector[(int)axisType];
    }
    
    public static Vector3 GetAxisVector(this Random axisRandom)
    {
        switch(axisRandom)
        {
            case Random.HORIZONTAL:
            default:
                return UnityEngine.Random.value > .5f ? Vector2.left : Vector2.right;
                
            case Random.VERTICAL:
                return UnityEngine.Random.value > .5f ? Vector2.up : Vector2.down;
        }
    }

    public static Vector2 GetDirection(Vector2 target, Vector2 origin)
    {
        Vector2 heading = target - origin;
        float distance = heading.magnitude;
        return  heading / distance;
    }
}