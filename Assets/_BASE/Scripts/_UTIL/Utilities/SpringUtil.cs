﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SpringUtil
{
    public static Vector3 SpringSemiImplicitEuler(Vector3 currentVector, ref Springing springingValues, Vector3 targetVector)
    {
        springingValues.velocity += -2f * Time.deltaTime * springingValues.damping * springingValues.AngularFrequency() * springingValues.velocity
            + Time.deltaTime * springingValues.AngularFrequency() * springingValues.AngularFrequency() * (targetVector - currentVector);
        return currentVector + (Time.deltaTime * springingValues.velocity);
    }
}

[System.Serializable]
public struct Springing
{
    public float damping;
    
#pragma warning disable 0649
    // remove 'this variable is never assigned to, and 
    // will always have its default value null' warning
    [SerializeField]
    private float angularFrequency;
#pragma warning restore 0649

    [HideInInspector]
    public Vector3 velocity;

    public float AngularFrequency()
    {
        return angularFrequency * Mathf.PI;
    }
}