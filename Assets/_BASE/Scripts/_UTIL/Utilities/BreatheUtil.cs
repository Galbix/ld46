﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MEC;

public static class BreatheUtil
{
    public enum SinWaveMode
    {
        NORMAL,
        SMOOTH,
        QUICK
    }

    static float GetSinWave(float myTheta, SinWaveMode myMode)
    {
        switch (myMode)
        {
            case SinWaveMode.SMOOTH:
                return Mathf.Sin(myTheta / 2);
            case SinWaveMode.QUICK:
                return 1.5f * Mathf.Sin(2 * myTheta);
        }

        return Mathf.Sin(myTheta);
    }

    #region Beathe Position
    public static CoroutineHandle BreathePosition(Transform targetTransform, Vector3 axisToModify, 
        float amplitude, float period, SinWaveMode waveMode = SinWaveMode.NORMAL)
    {
        IEnumerator<float> breathePos = BreathePositionCoroutine(targetTransform, axisToModify, amplitude, period, waveMode);
        return Timing.RunCoroutine(breathePos);
    }

    private static IEnumerator<float> BreathePositionCoroutine(Transform _targetTransform, Vector3 _axisToModify, 
        float _amplitude, float _period, SinWaveMode _waveMode = SinWaveMode.NORMAL)
    {
        float timer = 0f;
        Vector3 initialPos = _targetTransform.localPosition;

        while (true && _targetTransform)
        {
            timer += Time.deltaTime;
            float theta = timer / _period;
            float distance = _amplitude * GetSinWave(theta, _waveMode);
            _targetTransform.localPosition = initialPos + _axisToModify * distance;
            yield return Timing.WaitForOneFrame;
        }
    }
    #endregion

    #region Beathe Rotation
    public static CoroutineHandle BreatheRotation(Transform targetTransform, Vector3 axisToModify,
        float amplitude, float period, SinWaveMode waveMode = SinWaveMode.NORMAL)
    {
        IEnumerator<float> breatheRot = BreatheRotationCoroutine(targetTransform, axisToModify, amplitude, period, waveMode);
        return Timing.RunCoroutine(breatheRot);;
    }

    private static IEnumerator<float> BreatheRotationCoroutine(Transform _targetTransform, Vector3 _axisToModify,
        float _amplitude, float _period, SinWaveMode _waveMode = SinWaveMode.NORMAL)
    {
        float timer = 0f;
        Quaternion initialRot = _targetTransform.localRotation;

        while (true && _targetTransform)
        {
            timer += Time.deltaTime;
            float theta = timer / _period;
            float distance = _amplitude * GetSinWave(theta, _waveMode);
            _targetTransform.localRotation = Quaternion.Euler(initialRot.eulerAngles + _axisToModify * distance);
            yield return Timing.WaitForOneFrame;
        }
    }
    #endregion

    #region Beathe Scale
    public static CoroutineHandle BreatheScale(Transform targetTransform, Vector3 axisToModify,
        float amplitude, float period, SinWaveMode waveMode = SinWaveMode.NORMAL)
    {
        IEnumerator<float> breatheScale = BreatheScaleCoroutine(targetTransform, axisToModify, amplitude, period, waveMode);
        return Timing.RunCoroutine(breatheScale);
    }

    private static IEnumerator<float> BreatheScaleCoroutine(Transform _targetTransform, Vector3 _axisToModify, 
        float _amplitude, float _period, SinWaveMode _waveMode = SinWaveMode.NORMAL)
    {
        float timer = 0f;
        Vector3 initialPos = _targetTransform.localScale;

        while (true && _targetTransform)
        {
            timer += Time.deltaTime;
            float theta = timer / _period;
            float distance = _amplitude * GetSinWave(theta, _waveMode);
            _targetTransform.localScale = initialPos + _axisToModify * distance;
            yield return Timing.WaitForOneFrame;
        }
    }
    #endregion

    public static void EndCoroutine(CoroutineHandle cor)
    {
        Timing.KillCoroutines(cor);
    }
}
