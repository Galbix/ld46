﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using MEC;

public static class LerpUtil
{
    // TRANSLATE
    public static CoroutineHandle LerpMove(Transform _transformLerp, Vector3 _destPos, float _lerpTime, bool _isLocal = true)
    {
        return Timing.RunCoroutine(LerpMovement(_transformLerp, _destPos, _lerpTime, _isLocal = true));
    }
    
    public static CoroutineHandle LerpMove(Transform _transformLerp, Vector3 _destPos, float _lerpTime, AnimationCurve _ac, bool _isLocal = true)
    {
        return Timing.RunCoroutine(LerpMovement(_transformLerp, _destPos, _lerpTime, _ac, _isLocal));
    }
    
    public static CoroutineHandle LerpMove(Transform _transformLerp, Vector3 _destPos, TweenCustom tweenLerp, bool _isLocal = true)
    {
        return Timing.RunCoroutine(LerpMovement(_transformLerp, _destPos, tweenLerp.tweenTime, tweenLerp.tweenCurve, _isLocal));
    }
    
    public static CoroutineHandle LerpMove(RectTransform _transformLerp, Vector3 _destPos, float _lerpTime)
    {
        return Timing.RunCoroutine(LerpMovement(_transformLerp, _destPos, _lerpTime));
    }
    
    public static CoroutineHandle LerpMove(RectTransform _transformLerp, Vector3 _destPos, float _lerpTime, AnimationCurve _ac)
    {
        return Timing.RunCoroutine(LerpMovement(_transformLerp, _destPos, _lerpTime, _ac));
    }
    
    public static CoroutineHandle LerpMove(RectTransform _transformLerp, Vector3 _destPos, TweenCustom tweenLerp)
    {
        return Timing.RunCoroutine(LerpMovement(_transformLerp, _destPos, tweenLerp.tweenTime, tweenLerp.tweenCurve));
    }

    // ROTATE
    public static CoroutineHandle LerpRotation(Transform _transformLerp, Quaternion _destRot, float _lerpTime)
    {
        return Timing.RunCoroutine(LerpRotationCoroutine(_transformLerp, _destRot, _lerpTime));
    }
    
    public static CoroutineHandle LerpRotation(Transform _transformLerp, Quaternion _destRot, float _lerpTime, AnimationCurve _ac)
    {
        return Timing.RunCoroutine(LerpRotationCoroutine(_transformLerp, _destRot, _lerpTime, _ac));
    }
    
    public static CoroutineHandle LerpRotation(Transform _transformLerp, Quaternion _destRot, TweenCustom tweenLerp)
    {
        return Timing.RunCoroutine(LerpRotationCoroutine(_transformLerp, _destRot, tweenLerp.tweenTime, tweenLerp.tweenCurve));
    }
    
    public static CoroutineHandle LerpRotation(RectTransform _transformLerp, Quaternion _destRot, float _lerpTime)
    {
        return Timing.RunCoroutine(LerpRotationCoroutine(_transformLerp, _destRot, _lerpTime));
    }
    
    public static CoroutineHandle LerpRotation(RectTransform _transformLerp, Quaternion _destRot, float _lerpTime, AnimationCurve _ac)
    {
        return Timing.RunCoroutine(LerpRotationCoroutine(_transformLerp, _destRot, _lerpTime, _ac));
    }
    
    public static CoroutineHandle LerpRotation(RectTransform _transformLerp, Quaternion _destRot, TweenCustom tweenLerp)
    {
        return Timing.RunCoroutine(LerpRotationCoroutine(_transformLerp, _destRot, tweenLerp.tweenTime, tweenLerp.tweenCurve));
    }

    // SCALE
    public static CoroutineHandle LerpScale(Transform _transformLerp, Vector3 _destScale, float _lerpTime)
    {
        return Timing.RunCoroutine(LerpScaleCoroutine(_transformLerp, _destScale, _lerpTime));
    }
    
    public static CoroutineHandle LerpScale(Transform _transformLerp, Vector3 _destScale, float _lerpTime, AnimationCurve _ac)
    {
        return Timing.RunCoroutine(LerpScaleCoroutine(_transformLerp, _destScale, _lerpTime, _ac));
    }
    
    public static CoroutineHandle LerpScale(Transform _transformLerp, Vector3 _destScale, TweenCustom lerpTween)
    {
        return Timing.RunCoroutine(LerpScaleCoroutine(_transformLerp, _destScale, lerpTween.tweenTime, lerpTween.tweenCurve));
    }
    
    public static CoroutineHandle LerpScale(RectTransform _transformLerp, Vector3 _destScale, float _lerpTime)
    {
        return Timing.RunCoroutine(LerpScaleCoroutine(_transformLerp, _destScale, _lerpTime));
    }
    
    public static CoroutineHandle LerpScale(RectTransform _transformLerp, Vector3 _destScale, float _lerpTime, AnimationCurve _ac)
    {
        return Timing.RunCoroutine(LerpScaleCoroutine(_transformLerp, _destScale, _lerpTime, _ac));
    }
    
    public static CoroutineHandle LerpScale(RectTransform _transformLerp, Vector3 _destScale, TweenCustom lerpTween)
    {
        return Timing.RunCoroutine(LerpScaleCoroutine(_transformLerp, _destScale, lerpTween.tweenTime, lerpTween.tweenCurve));
    }

    // COLOR
    public static CoroutineHandle LerpColor(Text _text, Color _destColor, float _lerpTime)
    {
        return Timing.RunCoroutine(LerpColorCurve(_text, _destColor, _lerpTime));
    }
    
    public static CoroutineHandle LerpColor(Text _text, Color _destColor, float _lerpTime, bool _unscaledTime)
    {
        return Timing.RunCoroutine(LerpColorCurve(_text, _destColor, _lerpTime, _unscaledTime));
    }
    
    public static CoroutineHandle LerpColor(Text _text, Color _destColor, float _lerpTime, AnimationCurve _ac)
    {
        return Timing.RunCoroutine(LerpColorCurve(_text, _destColor, _lerpTime, _ac));
    }
    
    public static CoroutineHandle LerpColor(Text _text, Color _destColor, TweenCustom tweenLerp)
    {
        return Timing.RunCoroutine(LerpColorCurve(_text, _destColor, tweenLerp.tweenTime, tweenLerp.tweenCurve));
    }
    
    public static CoroutineHandle LerpColor(Image _img, Color _destColor, float _lerpTime)
    {
        return Timing.RunCoroutine(LerpColorCurve(_img, _destColor, _lerpTime));
    }
    
    public static CoroutineHandle LerpColor(Image _img, Color _destColor, float _lerpTime, bool _unscaledTime)
    {
        return Timing.RunCoroutine(LerpColorCurve(_img, _destColor, _lerpTime, _unscaledTime));
    }
    
    public static CoroutineHandle LerpColor(Image _img, Color _destColor, float _lerpTime, AnimationCurve _ac)
    {
        return Timing.RunCoroutine(LerpColorCurve(_img, _destColor, _lerpTime, _ac));
    }
    
    public static CoroutineHandle LerpColor(Image _img, Color _destColor, TweenCustom tweenLerp)
    {
        return Timing.RunCoroutine(LerpColorCurve(_img, _destColor, tweenLerp.tweenTime, tweenLerp.tweenCurve));
    }
    
    public static CoroutineHandle LerpColor(SpriteRenderer _sprite, Color _destColor, float _lerpTime)
    {
        return Timing.RunCoroutine(LerpColorCurve(_sprite, _destColor, _lerpTime));
    }
    
    public static CoroutineHandle LerpColor(SpriteRenderer _sprite, Color _destColor, float _lerpTime, AnimationCurve _ac)
    {
        return Timing.RunCoroutine(LerpColorCurve(_sprite, _destColor, _lerpTime, _ac));
    }
    
    public static CoroutineHandle LerpColor(SpriteRenderer _sprite, Color _destColor, TweenCustom tweenLerp)
    {
        return Timing.RunCoroutine(LerpColorCurve(_sprite, _destColor, tweenLerp.tweenTime, tweenLerp.tweenCurve));
    }
    
    public static CoroutineHandle LerpColor(Material _mat, Color _destColor, float _lerpTime)
    {
        return Timing.RunCoroutine(LerpColorCurve(_mat, _destColor, _lerpTime));
    }
    
    public static CoroutineHandle LerpColor(Material _mat, Color _destColor, float _lerpTime, AnimationCurve _ac)
    {
        return Timing.RunCoroutine(LerpColorCurve(_mat, _destColor, _lerpTime, _ac));
    }
    
    public static CoroutineHandle LerpColor(Material _mat, Color _destColor, TweenCustom tweenLerp)
    {
        return Timing.RunCoroutine(LerpColorCurve(_mat, _destColor, tweenLerp.tweenTime, tweenLerp.tweenCurve));
    }

    // SPRING
    public static void LerpPseudoSpring(Transform _transformLerp, Vector3 _maxScale, float _maxLerpTime, Vector3 _actualScale, float _actualLerpTime)
    {
        Timing.RunCoroutine(LerpScaleCoroutine(_transformLerp, _maxScale, _maxLerpTime));
        Timing.RunCoroutine(Spring(_transformLerp, _maxLerpTime, _actualScale, _actualLerpTime));
    }
    
    public static void LerpPseudoSpring(Transform _transformLerp, Vector3 _maxScale, float _maxLerpTime, Vector3 _actualScale, float _actualLerpTime, AnimationCurve _ac)
    {
        Timing.RunCoroutine(LerpScaleCoroutine(_transformLerp, _maxScale, _maxLerpTime, _ac));
        Timing.RunCoroutine(Spring(_transformLerp, _maxLerpTime, _actualScale, _actualLerpTime, _ac));
    }
    
    public static void LerpPseudoSpring(Transform _transformLerp, Vector3 _maxScale, float _maxLerpTime, Vector3 _actualScale, TweenCustom tweenLerp)
    {
        Timing.RunCoroutine(LerpScaleCoroutine(_transformLerp, _maxScale, _maxLerpTime, tweenLerp.tweenCurve));
        Timing.RunCoroutine(Spring(_transformLerp, _maxLerpTime, _actualScale, tweenLerp.tweenTime, tweenLerp.tweenCurve));
    }
    
    public static void LerpPseudoSpring(RectTransform _transformLerp, Vector3 _maxScale, float _maxLerpTime, Vector3 _actualScale, float _actualLerpTime)
    {
        Timing.RunCoroutine(LerpScaleCoroutine(_transformLerp, _maxScale, _maxLerpTime));
        Timing.RunCoroutine(Spring(_transformLerp, _maxLerpTime, _actualScale, _actualLerpTime));
    }
    
    public static void LerpPseudoSpring(RectTransform _transformLerp, Vector3 _maxScale, float _maxLerpTime, Vector3 _actualScale, float _actualLerpTime, AnimationCurve _ac)
    {
        Timing.RunCoroutine(LerpScaleCoroutine(_transformLerp, _maxScale, _maxLerpTime));
        Timing.RunCoroutine(Spring(_transformLerp, _maxLerpTime, _actualScale, _actualLerpTime, _ac));
    }
    
    public static void LerpPseudoSpring(RectTransform _transformLerp, Vector3 _maxScale, float _maxLerpTime, Vector3 _actualScale, TweenCustom tweenLerp)
    {
        Timing.RunCoroutine(LerpScaleCoroutine(_transformLerp, _maxScale, _maxLerpTime));
        Timing.RunCoroutine(Spring(_transformLerp, _maxLerpTime, _actualScale, tweenLerp.tweenTime, tweenLerp.tweenCurve));
    }


    #region LerpMovement
    private static IEnumerator<float> LerpMovement(Transform transformLerp, Vector3 destPos, float lerpTime, bool isLocal)
    {
        float initialTime = 0f;
        Vector3 initialPos = isLocal ? transformLerp.localPosition : transformLerp.position;

        while (initialTime < lerpTime && transformLerp)
        {
            initialTime += Time.deltaTime;

            if (isLocal)
                transformLerp.localPosition = Vector3.Lerp(initialPos, destPos, initialTime / lerpTime);
            else
                transformLerp.position = Vector3.Lerp(initialPos, destPos, initialTime / lerpTime);
            
            yield return Timing.WaitForOneFrame;
        }
    }

    private static IEnumerator<float> LerpMovement(RectTransform transformLerp, Vector3 destPos, float lerpTime)
    {
        float initialTime = 0f;
        Vector3 initialPos = transformLerp.anchoredPosition;

        while (initialTime < lerpTime && transformLerp)
        {
            initialTime += Time.deltaTime;
            transformLerp.anchoredPosition = Vector3.Lerp(initialPos, destPos, initialTime / lerpTime);
            yield return Timing.WaitForOneFrame;
        }
    }

    private static IEnumerator<float> LerpMovement(Transform transformLerp, Vector3 destPos, float lerpTime, AnimationCurve ac, bool isLocal)
    {
        float initialTime = 0f;
        Vector3 initialPos = isLocal ? transformLerp.localPosition : transformLerp.position;

        while (initialTime < lerpTime && transformLerp)
        {
            initialTime += Time.deltaTime;

            if (isLocal)
                transformLerp.localPosition = Vector3.Lerp(initialPos, destPos, ac.Evaluate(initialTime / lerpTime));
            else
                transformLerp.position = Vector3.Lerp(initialPos, destPos, ac.Evaluate(initialTime / lerpTime));

            yield return Timing.WaitForOneFrame;
        }
    }

    private static IEnumerator<float> LerpMovement(RectTransform transformLerp, Vector3 destPos, float lerpTime, AnimationCurve ac)
    {
        float initialTime = 0f;
        Vector3 initialPos = transformLerp.anchoredPosition;

        while (initialTime < lerpTime && transformLerp)
        {
            initialTime += Time.deltaTime;
            transformLerp.anchoredPosition = Vector3.Lerp(initialPos, destPos, ac.Evaluate(initialTime / lerpTime));
            yield return Timing.WaitForOneFrame;
        }
    }
    #endregion

    #region LerpRotation
    private static IEnumerator<float> LerpRotationCoroutine(Transform transformLerp, Quaternion destRotation, float lerpTime)
    {
        float initialTime = 0f;
        Quaternion initialRotation = transformLerp.localRotation;

        while (initialTime < lerpTime && transformLerp)
        {
            initialTime += Time.deltaTime;
            transformLerp.localRotation = Quaternion.Lerp(initialRotation, destRotation, initialTime / lerpTime);
            yield return Timing.WaitForOneFrame;
        }
    }

    private static IEnumerator<float> LerpRotationCoroutine(RectTransform transformLerp, Quaternion destRotation, float lerpTime)
    {
        float initialTime = 0f;
        Quaternion initialRotation = transformLerp.localRotation;

        while (initialTime < lerpTime && transformLerp)
        {
            initialTime += Time.deltaTime;
            transformLerp.localRotation = Quaternion.Lerp(initialRotation, destRotation, initialTime / lerpTime);
            yield return Timing.WaitForOneFrame;
        }
    }

    private static IEnumerator<float> LerpRotationCoroutine(Transform transformLerp, Quaternion destRotation, float lerpTime, AnimationCurve ac)
    {
        float initialTime = 0f;
        Quaternion initialRotation = transformLerp.localRotation;

        while (initialTime < lerpTime && transformLerp)
        {
            initialTime += Time.deltaTime;
            transformLerp.localRotation = Quaternion.Lerp(initialRotation, destRotation, ac.Evaluate(initialTime / lerpTime));
            yield return Timing.WaitForOneFrame;
        }
    }

    private static IEnumerator<float> LerpRotationCoroutine(RectTransform transformLerp, Quaternion destRotation, float lerpTime, AnimationCurve ac)
    {
        float initialTime = 0f;
        Quaternion initialRotation = transformLerp.localRotation;

        while (initialTime < lerpTime && transformLerp)
        {
            initialTime += Time.deltaTime;
            transformLerp.localRotation = Quaternion.Lerp(initialRotation, destRotation, ac.Evaluate(initialTime / lerpTime));
            yield return Timing.WaitForOneFrame;
        }
    }
    #endregion

    #region LerpScale
    private static IEnumerator<float> LerpScaleCoroutine(Transform transformLerp, Vector3 destScale, float lerpTime)
    {
        float initialTime = 0f;
        Vector3 initialScale = transformLerp.localScale;

        while (initialTime < lerpTime && transformLerp)
        {
            initialTime += Time.deltaTime;
            transformLerp.localScale = Vector3.Lerp(initialScale, destScale, initialTime / lerpTime);
            yield return Timing.WaitForOneFrame;
        }
    }

    private static IEnumerator<float> LerpScaleCoroutine(RectTransform transformLerp, Vector3 destScale, float lerpTime)
    {
        float initialTime = 0f;
        Vector3 initialScale = transformLerp.localScale;

        while (initialTime < lerpTime && transformLerp)
        {
            initialTime += Time.deltaTime;
            transformLerp.localScale = Vector3.Lerp(initialScale, destScale, initialTime / lerpTime);
            yield return Timing.WaitForOneFrame;
        }
    }

    private static IEnumerator<float> LerpScaleCoroutine(Transform transformLerp, Vector3 destScale, float lerpTime, AnimationCurve ac)
    {
        float initialTime = 0f;
        Vector3 initialScale = transformLerp.localScale;

        while (initialTime < lerpTime && transformLerp)
        {
            initialTime += Time.deltaTime;
            transformLerp.localScale = Vector3.Lerp(initialScale, destScale, ac.Evaluate(initialTime / lerpTime));
            yield return Timing.WaitForOneFrame;
        }
    }

    private static IEnumerator<float> LerpScaleCoroutine(RectTransform transformLerp, Vector3 destScale, float lerpTime, AnimationCurve ac)
    {
        float initialTime = 0f;
        Vector3 initialScale = transformLerp.localScale;

        while (initialTime < lerpTime && transformLerp)
        {
            initialTime += Time.deltaTime;
            transformLerp.localScale = Vector3.Lerp(initialScale, destScale, ac.Evaluate(initialTime / lerpTime));
            yield return Timing.WaitForOneFrame;
        }
    }
    #endregion

    #region LerpColor
    private static IEnumerator<float> LerpColorCurve(Text text, Color destColor, float lerpTime)
    {
        float initialTime = 0f;
        Color initialColor = text.color;

        while (initialTime < lerpTime && text)
        {
            initialTime += Time.deltaTime;
            text.color = Color.Lerp(initialColor, destColor, initialTime / lerpTime);
            yield return Timing.WaitForOneFrame;
        }
    }
    
    private static IEnumerator<float> LerpColorCurve(Text text, Color destColor, float lerpTime, bool unscaledTime)
    {
        float initialTime = 0f;
        Color initialColor = text.color;

        while (initialTime < lerpTime && text)
        {
            initialTime = unscaledTime ? initialTime + Time.unscaledDeltaTime : initialTime + Time.deltaTime;
            text.color = Color.Lerp(initialColor, destColor, initialTime / lerpTime);
            yield return Timing.WaitForOneFrame;
        }
    }
    
    private static IEnumerator<float> LerpColorCurve(Text text, Color destColor, float lerpTime, AnimationCurve ac)
    {
        float initialTime = 0f;
        Color initialColor = text.color;

        while (initialTime < lerpTime && text)
        {
            initialTime += Time.deltaTime;
            text.color = Color.Lerp(initialColor, destColor, ac.Evaluate(initialTime / lerpTime));
            yield return Timing.WaitForOneFrame;
        }
    }

    private static IEnumerator<float> LerpColorCurve(Image img, Color destColor, float lerpTime)
    {
        float initialTime = 0f;
        Color initialColor = img.color;

        while (initialTime < lerpTime && img)
        {
            initialTime += Time.deltaTime;
            img.color = Color.Lerp(initialColor, destColor, initialTime / lerpTime);
            yield return Timing.WaitForOneFrame;
        }
    }
    
    private static IEnumerator<float> LerpColorCurve(Image img, Color destColor, float lerpTime, bool unscaledTime)
    {
        float initialTime = 0f;
        Color initialColor = img.color;

        while (initialTime < lerpTime && img)
        {
            initialTime = unscaledTime ? initialTime + Time.unscaledDeltaTime : initialTime + Time.deltaTime;
            img.color = Color.Lerp(initialColor, destColor, initialTime / lerpTime);
            yield return Timing.WaitForOneFrame;
        }
    }
    
    private static IEnumerator<float> LerpColorCurve(Image img, Color destColor, float lerpTime, AnimationCurve ac)
    {
        float initialTime = 0f;
        Color initialColor = img.color;

        while (initialTime < lerpTime && img)
        {
            initialTime += Time.deltaTime;
            img.color = Color.Lerp(initialColor, destColor, ac.Evaluate(initialTime / lerpTime));
            yield return Timing.WaitForOneFrame;
        }
    }

    private static IEnumerator<float> LerpColorCurve(SpriteRenderer sprite, Color destColor, float lerpTime)
    {
        float initialTime = 0f;
        Color initialColor = sprite.color;

        while (initialTime < lerpTime && sprite)
        {
            initialTime += Time.deltaTime;
            sprite.color = Color.Lerp(initialColor, destColor, initialTime / lerpTime);
            yield return Timing.WaitForOneFrame;
        }
    }
    
    private static IEnumerator<float> LerpColorCurve(SpriteRenderer sprite, Color destColor, float lerpTime, AnimationCurve ac)
    {
        float initialTime = 0f;
        Color initialColor = sprite.color;

        while (initialTime < lerpTime && sprite)
        {
            initialTime += Time.deltaTime;
            sprite.color = Color.Lerp(initialColor, destColor, ac.Evaluate(initialTime / lerpTime));
            yield return Timing.WaitForOneFrame;
        }
    }

    private static IEnumerator<float> LerpColorCurve(Material mat, Color destColor, float lerpTime)
    {
        float initialTime = 0f;
        Color initialColor = mat.color;

        while (initialTime < lerpTime && mat)
        {
            initialTime += Time.deltaTime;
            mat.color = Color.Lerp(initialColor, destColor, initialTime / lerpTime);
            yield return Timing.WaitForOneFrame;
        }
    }
    
    private static IEnumerator<float> LerpColorCurve(Material mat, Color destColor, float lerpTime, AnimationCurve ac)
    {
        float initialTime = 0f;
        Color initialColor = mat.color;

        while (initialTime < lerpTime && mat)
        {
            initialTime += Time.deltaTime;
            mat.color = Color.Lerp(initialColor, destColor, ac.Evaluate(initialTime / lerpTime));
            yield return Timing.WaitForOneFrame;
        }
    }
    #endregion

    #region LerpSpring
    private static IEnumerator<float> Spring(Transform _transformLerp, float springTime, Vector3 _actualScale, float _actualLerpTime)
    {
        yield return Timing.WaitForSeconds(springTime);
        Timing.RunCoroutine(LerpScaleCoroutine(_transformLerp, _actualScale, _actualLerpTime));
    }
    
    private static IEnumerator<float> Spring(Transform _transformLerp, float springTime, Vector3 _actualScale, float _actualLerpTime, AnimationCurve _ac)
    {
        yield return Timing.WaitForSeconds(springTime);
        Timing.RunCoroutine(LerpScaleCoroutine(_transformLerp, _actualScale, _actualLerpTime, _ac));
    }
    
    private static IEnumerator<float> Spring(RectTransform _transformLerp, float springTime, Vector3 _actualScale, float _actualLerpTime)
    {
        yield return Timing.WaitForSeconds(springTime);
        Timing.RunCoroutine(LerpScaleCoroutine(_transformLerp, _actualScale, _actualLerpTime));
    }
    
    private static IEnumerator<float> Spring(RectTransform _transformLerp, float springTime, Vector3 _actualScale, float _actualLerpTime, AnimationCurve _ac)
    {
        yield return Timing.WaitForSeconds(springTime);
        Timing.RunCoroutine(LerpScaleCoroutine(_transformLerp, _actualScale, _actualLerpTime, _ac));
    }
    #endregion
}
