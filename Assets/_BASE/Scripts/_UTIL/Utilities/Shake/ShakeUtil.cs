﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public static class ShakeUtil
{
    public static CoroutineHandle ExecuteShakeLoop(Transform shakeTransform, float amount)
    {
        IEnumerator<float> executeShakeLoop = ExecuteShakeLoopCoroutine(shakeTransform, amount);
        return Timing.RunCoroutine(executeShakeLoop);
    }

    private static IEnumerator<float> ExecuteShakeLoopCoroutine(Transform _shakeTransform, float _amount)
    {
        Vector3 originalPos = _shakeTransform.localPosition;

        while (true && _shakeTransform)
        {
            _shakeTransform.localPosition = originalPos + Random.insideUnitSphere * _amount;
            yield return Timing.WaitForOneFrame;
        }
    }

    public static CoroutineHandle ExecuteShake(Transform shakeTransform, float time, float amount, float decrease)
    {
        IEnumerator<float> executeShake = ExecuteShakeCoroutine(shakeTransform, time, amount, decrease);
        return Timing.RunCoroutine(executeShake);
    }

    private static IEnumerator<float> ExecuteShakeCoroutine(Transform _shakeTransform, float _time, float _amount, float _decrease)
    {
        Vector3 originalPos = _shakeTransform.localPosition;

        while (_time > 0f)
        {
            _shakeTransform.localPosition = originalPos + Random.insideUnitSphere * _amount;
            _time -= Time.deltaTime * _decrease;
            yield return Timing.WaitForOneFrame;
        }

        _shakeTransform.localPosition = originalPos;
    }

    public static void ExecuteTimedShake(Transform shakeTransform, float time, float amount, float decrease, Vector3 direction)
    {
        Timing.RunCoroutine(ExecuteTimedShakeCoroutine(shakeTransform, time, amount, decrease, direction));
    }
    
    public static void ExecuteTimedShake(Transform shakeTransform, Shake shake)
    {
        Timing.RunCoroutine(ExecuteTimedShakeCoroutine(shakeTransform, shake.shakeTime, shake.shakeAmount, shake.shakeDecrease, shake.shakeDir));
    }

    private static IEnumerator<float> ExecuteTimedShakeCoroutine(Transform _shakeTransform, float _time, float _amount, float _decrease, Vector3 _direction)
    {
        Vector3 originalPos = _shakeTransform.localPosition;
        Vector3 randomPoint;

        while (_time > 0f)
        {
            randomPoint = Vector3.Scale(Random.insideUnitSphere, _direction);
            _shakeTransform.localPosition = originalPos + randomPoint * _amount;
            _time -= Time.deltaTime * _decrease;
            yield return Timing.WaitForOneFrame;
        }

        _shakeTransform.localPosition = originalPos;
    }

    public static void ExecuteTimedShake(RectTransform shakeTransform, float time, float amount, float decrease, Vector3 direction)
    {
        Timing.RunCoroutine(ExecuteTimedShakeCoroutine(shakeTransform, time, amount, decrease, direction));
    }
    
    public static void ExecuteTimedShake(RectTransform shakeTransform, Shake shake)
    {
        Timing.RunCoroutine(ExecuteTimedShakeCoroutine(shakeTransform, shake.shakeTime, shake.shakeAmount, shake.shakeDecrease, shake.shakeDir));
    }

    private static IEnumerator<float> ExecuteTimedShakeCoroutine(RectTransform _shakeTransform, float _time, float _amount, float _decrease, Vector3 _direction)
    {
        Vector3 originalPos = _shakeTransform.anchoredPosition;
        Vector3 randomPoint;

        while (_time > 0f)
        {
            randomPoint = Vector3.Scale(Random.insideUnitSphere, _direction);
            _shakeTransform.anchoredPosition = originalPos + randomPoint * _amount;
            _time -= Time.deltaTime * _decrease;
            yield return Timing.WaitForOneFrame;
        }

        _shakeTransform.anchoredPosition = originalPos;
    }
}
