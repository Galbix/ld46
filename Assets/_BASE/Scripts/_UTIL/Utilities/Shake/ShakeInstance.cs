﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public class ShakeInstance
{
    private bool shakeActive = true;
    private IEnumerator<float> executeTimedShake;
    private CoroutineHandle coroutineTimedShake;
    private Vector3 originalPos;

    public CoroutineHandle ExecuteShakeLoop(Transform shakeTransform, float amount, float delay, Vector3 direction)
    {
        IEnumerator<float> executeShakeLoop = ExecuteShakeLoopCoroutine(shakeTransform, amount, delay, direction);
        return Timing.RunCoroutine(executeShakeLoop);
    }
    
    public CoroutineHandle ExecuteShakeLoop(RectTransform shakeTransform, float amount, float delay, Vector3 direction)
    {
        IEnumerator<float> executeShakeLoop = ExecuteShakeLoopCoroutine(shakeTransform, amount, delay, direction);
        return Timing.RunCoroutine(executeShakeLoop);
    }

    public CoroutineHandle ExecuteShakeLoop(Transform shakeTransform, float amount, float delay, Vector3 direction, AnimationCurve ac)
    {
        IEnumerator<float> executeShakeLoop = ExecuteShakeLoopCoroutine(shakeTransform, amount, delay, direction, ac);
        return Timing.RunCoroutine(executeShakeLoop);
    }
    
    public CoroutineHandle ExecuteShakeLoop(RectTransform shakeTransform, float amount, float delay, Vector3 direction, AnimationCurve ac)
    {
        IEnumerator<float> executeShakeLoop = ExecuteShakeLoopCoroutine(shakeTransform, amount, delay, direction, ac);
        return Timing.RunCoroutine(executeShakeLoop);
    }

    public void ExecuteTimedShake(Transform shakeTransform, float time, float amount, float decrease, Vector3 direction)
    {
        if (coroutineTimedShake != null)
        {
            Timing.KillCoroutines(coroutineTimedShake);
            EndTimedShake(shakeTransform);
        }

        executeTimedShake = ExecuteTimedShakeCoroutine(shakeTransform, time, amount, decrease, direction);
        coroutineTimedShake = Timing.RunCoroutine(executeTimedShake);
    }
    
    public void ExecuteTimedShake(Transform shakeTransform, Shake shake)
    {
        if (executeTimedShake != null)
        {
            Timing.KillCoroutines(coroutineTimedShake);
            EndTimedShake(shakeTransform);
        }

        executeTimedShake = ExecuteTimedShakeCoroutine(shakeTransform, shake.shakeTime, shake.shakeAmount, shake.shakeDecrease, shake.shakeDir);
        coroutineTimedShake = Timing.RunCoroutine(executeTimedShake);
    }

    private IEnumerator<float> ExecuteTimedShakeCoroutine(Transform _shakeTransform, float _time, float _amount, float _decrease, Vector3 _direction)
    {
        originalPos = _shakeTransform.localPosition;
        Vector3 randomPoint;

        while (_time > 0f)
        {
            randomPoint = Vector3.Scale(Random.insideUnitSphere, _direction);
            _shakeTransform.localPosition = originalPos + randomPoint * _amount;
            _time -= Time.deltaTime * _decrease;
            yield return Timing.WaitForOneFrame;
        }

        EndTimedShake(_shakeTransform);
    }

    private void EndTimedShake(Transform _shakeTransform)
    {
        _shakeTransform.localPosition = originalPos;
    }

    private IEnumerator<float> ExecuteShakeLoopCoroutine(Transform _shakeTransform, float _amount, float _delay, Vector3 _direction)
    {
        Vector3 originalPos = _shakeTransform.localPosition;
        Vector3 randomPoint, targetPos;

        while (_shakeTransform)
        {
            if (shakeActive)
            {
                randomPoint = Vector3.Scale(Random.insideUnitSphere, _direction);
                targetPos = originalPos + randomPoint * _amount;
                LerpUtil.LerpMove(_shakeTransform, targetPos, _delay, true);
                yield return Timing.WaitForSeconds(_delay);
            }
            yield return Timing.WaitForOneFrame;
        }
    }
    
    private IEnumerator<float> ExecuteShakeLoopCoroutine(Transform _shakeTransform, float _amount, float _delay, Vector3 _direction, AnimationCurve _ac)
    {
        Vector3 originalPos = _shakeTransform.localPosition;
        Vector3 randomPoint, targetPos;

        while (_shakeTransform)
        {
            if (shakeActive)
            {
                randomPoint = Vector3.Scale(Random.insideUnitSphere, _direction);
                targetPos = originalPos + randomPoint * _amount;
                LerpUtil.LerpMove(_shakeTransform, targetPos, _delay, _ac, true);
                yield return Timing.WaitForSeconds(_delay);
            }
            yield return Timing.WaitForOneFrame;
        }
    }
    
    private IEnumerator<float> ExecuteShakeLoopCoroutine(RectTransform _shakeTransform, float _amount, float _delay, Vector3 _direction)
    {
        Vector3 originalPos = _shakeTransform.anchoredPosition;
        Vector3 randomPoint, targetPos;

        while (_shakeTransform)
        {
            if (shakeActive)
            {
                randomPoint = Vector3.Scale(Random.insideUnitSphere, _direction);
                targetPos = originalPos + randomPoint * _amount;
                LerpUtil.LerpMove(_shakeTransform, targetPos, _delay, true);
                yield return Timing.WaitForSeconds(_delay);
            }
            yield return Timing.WaitForOneFrame;
        }
    }
    
    private IEnumerator<float> ExecuteShakeLoopCoroutine(RectTransform _shakeTransform, float _amount, float _delay, Vector3 _direction, AnimationCurve _ac)
    {
        Vector3 originalPos = _shakeTransform.anchoredPosition;
        Vector3 randomPoint, targetPos;

        while (_shakeTransform)
        {
            if (shakeActive)
            {
                randomPoint = Vector3.Scale(Random.insideUnitSphere, _direction);
                targetPos = originalPos + randomPoint * _amount;
                LerpUtil.LerpMove(_shakeTransform, targetPos, _delay, _ac, true);
                yield return Timing.WaitForSeconds(_delay);
            }
            yield return Timing.WaitForOneFrame;
        }
    }

    public void StopShakeMovement()
    {
        shakeActive = false;
    }
    
    public void ResumeShakeMovement()
    {
        shakeActive = true;
    }
}
