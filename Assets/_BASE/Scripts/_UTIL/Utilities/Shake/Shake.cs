﻿using UnityEngine;

[System.Serializable]
public class Shake
{
    public Transform targetTransform;

    [Header("Basic Shake")]
    public float shakeAmount;
    public Vector3 shakeDir = Vector3.one;

    [Header("Timed Shake")]
    public float shakeTime = 0.2f;
    public float shakeDecrease = 1f;

    [Header("Loop Shake")]
    public bool startLoopShake = false;
    public bool endLoopShake = false;
    [Range(.02f, .5f)] public float shakeDelay = .05f;
    public AnimationCurve shakeCurve;
}
