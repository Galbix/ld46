﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class QuaternionUtil
{
    public static Quaternion GetFacingDirection2D(Vector2 directionToTarget) // not the target's position, the direction!
    {
        return Quaternion.LookRotation(Vector3.forward, directionToTarget);
    }
}
