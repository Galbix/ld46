﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MEC;

public static class BezierUtil
{
    public static IEnumerator<float> MovementBezier(Transform bezierTransform, Vector3 bezierStartPoint,
        Vector3 bezierControlPoint, Vector3 bezierEndPoint, float bezierTotalTime)
    {
        IEnumerator<float> moveBezier = MovementBezierCoroutine(bezierTransform, bezierStartPoint,
            bezierControlPoint, bezierEndPoint, bezierTotalTime);
        return moveBezier;
    }

    public static IEnumerator<float> MovementBezier(Transform bezierTransform, Vector3 bezierStartPoint,
        Vector3 bezierControlPoint, Vector3 bezierEndPoint, float bezierTotalTime, AnimationCurve animationCurve)
    {
        IEnumerator<float> moveBezier = MovementBezierCoroutine(bezierTransform, bezierStartPoint,
            bezierControlPoint, bezierEndPoint, bezierTotalTime, animationCurve);
        return moveBezier;
    }

    public static IEnumerator<float> MovementBezierCoroutine(Transform _bezierTransform, Vector3 _bezierStartPoint, 
        Vector3 _bezierControlPoint, Vector3 _bezierEndPoint, float _bezierTotalTime)
    {
        float bezierTime = 0f;

        while (bezierTime < _bezierTotalTime)
        {
            bezierTime += Time.deltaTime;
            _bezierTransform.position = QuadraticBezier(_bezierStartPoint, _bezierControlPoint,
                _bezierEndPoint, bezierTime / _bezierTotalTime);
            yield return Timing.WaitForOneFrame;
        }

        _bezierTransform.position = _bezierEndPoint;
    }

    public static IEnumerator<float> MovementBezierCoroutine(Transform _bezierTransform, Vector3 _bezierStartPoint,
        Vector3 _bezierControlPoint, Vector3 _bezierEndPoint, float _bezierTotalTime, AnimationCurve _animationCurve)
    {
        float bezierTime = 0f;

        while (bezierTime < _bezierTotalTime)
        {
            bezierTime += Time.deltaTime;
            _bezierTransform.position = QuadraticBezier(_bezierStartPoint, _bezierControlPoint, 
                _bezierEndPoint, _animationCurve.Evaluate(bezierTime / _bezierTotalTime));
            yield return Timing.WaitForOneFrame;
        }

        _bezierTransform.position = _bezierEndPoint;
    }

    private static Vector3 QuadraticBezier(Vector3 startP, Vector3 controlP, Vector3 endP, float bTime)
    {
        Vector3 quadraticCurve = new Vector3();

        quadraticCurve.x =
                (
                    ((1 - bTime) * (1 - bTime)) * startP.x
                )
            +
                (
                    2 * bTime * (1 - bTime) * controlP.x
                )
            +
                (
                    (bTime * bTime) * endP.x
                );

        quadraticCurve.y =
                (
                    ((1 - bTime) * (1 - bTime)) * startP.y
                )
            +
                (
                    2 * bTime * (1 - bTime) * controlP.y
                )
            +
                (
                    (bTime * bTime) * endP.y
                );

        quadraticCurve.z = 0f;

        return quadraticCurve;
    }

}
