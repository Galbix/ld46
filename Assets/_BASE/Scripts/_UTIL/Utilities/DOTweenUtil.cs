﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace DG.Tweening
{

public static class DOTweenUtil
{
    public static Tweener DOPunchScale(this Transform transform, DOTweenPunchData punchData)
    {
        return transform.DOPunchScale(punchData);
    }
}

public class DOTweenPunchData
{
    public Vector3 punch;
    public float duration;
    public int vibrato = 10;
    public float elasticity = 1f;
}

}