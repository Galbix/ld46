﻿using UnityEngine;
using System.Collections;

public static class CanvasUtil
{
    #region GetCanvasPosition
    // Returns the position in the canvas of a specific point
    public static Vector3 GetCanvasPosition(RectTransform canvasR, Vector3 refPos)
    {
        Vector2 viewportPosition = Camera.main.WorldToViewportPoint(refPos);

        Vector2 worldObject_ScreenPosition = new Vector2(
            ((viewportPosition.x * canvasR.sizeDelta.x) - (canvasR.sizeDelta.x * 0.5f)),
            ((viewportPosition.y * canvasR.sizeDelta.y) - (canvasR.sizeDelta.y * 0.5f))
        );

        return worldObject_ScreenPosition;
    }

    public static Vector3 GetCanvasPosition(RectTransform canvasR, Vector3 refPos, float extraX, float extraY)
    {
        Vector2 viewportPosition = Camera.main.WorldToViewportPoint(refPos);

        Vector2 worldObject_ScreenPosition = new Vector2(
            ((viewportPosition.x * canvasR.sizeDelta.x) - (canvasR.sizeDelta.x * 0.5f)) + extraX,
            ((viewportPosition.y * canvasR.sizeDelta.y) - (canvasR.sizeDelta.y * 0.5f)) + extraY
        );

        return worldObject_ScreenPosition;
    }
    #endregion
}
