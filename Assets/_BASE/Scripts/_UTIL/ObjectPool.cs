﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class ObjectPool : MonoBehaviour
{
    public GameObject prefab;
    public int size;
    public ParentType parentType;

    public enum ParentType
    {
        NONE,
        CREATE_CUSTOM,
        THIS_GAMEOBJECT,
        EXISTING_GAMEOBJECT
    }

    // CREATE CUSTOM
    public bool IsParentTypeCreateCustom()
    {
        return parentType.Equals(ParentType.CREATE_CUSTOM);
    }

    [ShowIf("IsParentTypeCreateCustom")]
    public string customParentName;

    // THIS GAMEOBJECT
    public bool IsParentTypeThisGO()
    {
        return parentType.Equals(ParentType.THIS_GAMEOBJECT);
    }

    // EXISTING GAMEOBJECT
    public bool IsParentTypeExistingGO()
    {
        return parentType.Equals(ParentType.EXISTING_GAMEOBJECT);
    }

    [ShowIf("IsParentTypeExistingGO")]
    public Transform existingParentTransform;

    private List<GameObject> pool = new List<GameObject>();
    
    private Transform parentTransform;
    
    private bool isOnCanvas = false;

    private void Awake()
    {
        isOnCanvas = GetComponent<RectTransform>();
        
        InitializePool();
    }

    private void InitializePool()
    {
        parentTransform = GetParentTransform();

        for (int i = 0; i < size; i++)
        {
            GameObject currentObject = Instantiate(prefab, transform.position, Quaternion.identity) as GameObject;
            currentObject.transform.SetParent(parentTransform, ShouldWorldPositionStayOnSetParent());
            AddObject(currentObject);
        }
    }
    
    private bool ShouldWorldPositionStayOnSetParent()
    {
        return !isOnCanvas;
    }

    public Transform GetParentTransform()
    {
        switch(parentType)
        {
            case ParentType.NONE:
            default:
                return null;

            case ParentType.CREATE_CUSTOM:
                return new GameObject(customParentName).transform;

            case ParentType.THIS_GAMEOBJECT:
                return this.transform;

            case ParentType.EXISTING_GAMEOBJECT:
                return existingParentTransform;
        }
    }

    public GameObject GetObject()
    {
        GameObject chosenObject = pool[0];
        RemoveObject(chosenObject);
        return chosenObject;
    }

    public void AddObject(GameObject currentObject)
    {
        pool.Add(currentObject);
        currentObject.SetActive(false);
    }

    private void RemoveObject(GameObject currentObject)
    {
        pool.Remove(currentObject);
        currentObject.SetActive(true);
    }
    
    public void DestroyPool()
    {
        Destroy(parentTransform.gameObject);
    }
}
