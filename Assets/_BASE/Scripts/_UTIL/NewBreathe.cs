﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;
using Sirenix.OdinInspector;

public class NewBreathe : SerializedMonoBehaviour
{
    public List<BreatheModifierBase> breatheModifiers = new List<BreatheModifierBase>();
    
    private List<CoroutineHandle> activeModifiers = new List<CoroutineHandle>();
    
    private void Start() 
    {
        ApplyModifiers();
    }
    
    private void ApplyModifiers()
    {
        for(int i = 0; i < breatheModifiers.Count; i++)
        {
            activeModifiers.Add(breatheModifiers[i].Apply(this.transform));
        }
    }    
}

[System.Serializable]
public abstract class BreatheModifierBase
{
    [Title("Amplitude")]
    public bool useRandomAmplitude;
    
    [HideIf("useRandomAmplitude")]
    public float amplitude;
    
    [ShowIf("useRandomAmplitude")]
    public float minAmplitude;
    
    [ShowIf("useRandomAmplitude")]
    public float maxAmplitude;
    
    
    [Title("Period")]
    public bool useRandomPeriod;
    
    [HideIf("useRandomPeriod")]
    public float period;
    
    [ShowIf("useRandomPeriod")]
    public float minPeriod;
    
    [ShowIf("useRandomPeriod")]
    public float maxPeriod;
    
    
    [Title("Axis To Modify")]
    public bool useRandomAxis;
    
    [HideIf("useRandomAxis")]
    public bool useCustomVector = false;
    
    [ShowIf("UseAxisToModify")]
    public VectorUtil.AxisType axisTypeToModify;
    
    [ShowIf("UseCustomVector")]
    public Vector3 vectorToModify;
    
    [ShowIf("useRandomAxis")]
    public VectorUtil.Random axisRandom;
    
    [Title("Transform Target")]
    public bool useCustomTransform;
    [ShowIf("useCustomTransform")]
    public Transform customTransform;
    
    public BreatheModifierBase()
    {
        axisTypeToModify = GetDefaultAxisToModifyValue();
    }
    
    protected abstract VectorUtil.AxisType GetDefaultAxisToModifyValue();
    
    protected Vector3 GetAxisToModify()
    {
        if(useRandomAxis)
        {
            return axisRandom.GetAxisVector();
        }
        
        return useCustomVector ? vectorToModify : axisTypeToModify.GetAxisVector();
    }
    
    protected Transform GetTargetTransform(Transform targetTransform)
    {
        if(useCustomTransform)
        {
            return customTransform;
        }
        
        return targetTransform;
    }
    
    public abstract CoroutineHandle Apply(Transform targetTransform);
    
    public float GetAmplitude()
    {
        if(useRandomAmplitude)
        {
            return Random.Range(minAmplitude, maxAmplitude);
        }
        
        return amplitude;
    }
    
    public float GetPeriod()
    {
        if(useRandomPeriod)
        {
            return Random.Range(minPeriod, maxPeriod);
        }
        
        return period;
    }
    
    public bool UseAxisToModify()
    {
        if(useRandomAxis)
        {
            return false;
        }
        
        if(useCustomVector)
        {
            return false;
        }
        
        return true;
    }
    
    public bool UseCustomVector()
    {
        if(useRandomAxis)
        {
            return false;
        }
        
        return useCustomVector;
    }
}

[System.Serializable]
public class BreatheModifierPosition : BreatheModifierBase
{
    protected override VectorUtil.AxisType GetDefaultAxisToModifyValue()
    {
        return VectorUtil.AxisType.UP;
    }
    
    public override CoroutineHandle Apply(Transform targetTransform)
    {
        return BreatheUtil.BreathePosition(GetTargetTransform(targetTransform),
                GetAxisToModify(), GetAmplitude(), GetPeriod());
    }
}

[System.Serializable]
public class BreatheModifierRotation : BreatheModifierBase
{
    protected override VectorUtil.AxisType GetDefaultAxisToModifyValue()
    {
        return VectorUtil.AxisType.FORWARD;
    }
    
    public override CoroutineHandle Apply(Transform targetTransform)
    {
        return BreatheUtil.BreatheRotation(GetTargetTransform(targetTransform),
                GetAxisToModify(), GetAmplitude(), GetPeriod());
    }
}

[System.Serializable]
public class BreatheModifierScale : BreatheModifierBase
{
    protected override VectorUtil.AxisType GetDefaultAxisToModifyValue()
    {
        return VectorUtil.AxisType.ONE;
    }
    
    public override CoroutineHandle Apply(Transform targetTransform)
    {
        return BreatheUtil.BreatheScale(GetTargetTransform(targetTransform),
                GetAxisToModify(), GetAmplitude(), GetPeriod());
    }
}
