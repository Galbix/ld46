﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorCustom : Singleton<CursorCustom>
{
    public Camera mainCamera;

    private bool forcePosition = false;

    private Vector3 lastMousePos;
    private Vector3 mouseDelta;
    private float mouseSpeed;

    #region UNITY METHODS
    private void Start()
    {
        Cursor.visible = false;
    }

    private void LateUpdate()
    {
        mouseDelta = Input.mousePosition - lastMousePos;
        mouseSpeed = mouseDelta.magnitude;
        lastMousePos = Input.mousePosition;

        if(ShouldFollowCursor())
        {
            FollowCursor();
        }
    }
    #endregion

    private bool ShouldFollowCursor()
    {
        if(forcePosition)
        {
            return false;
        }

        return true;
    }
    
    private void FollowCursor()
    {
        this.transform.position = GetCanvasPosition();
    }
    
    private Vector3 GetCanvasPosition()
    {
        return new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f);
    }
    
    public Vector2 GetWorldPosition()
    {
        return mainCamera.ScreenToWorldPoint(this.transform.position);
    }

    public void ForcePosition(Vector3 newPosition)
    {
        forcePosition = true;
        this.transform.position = mainCamera.WorldToScreenPoint(newPosition.WithZ(10f));
    }

    public void EndForcePosition()
    {
        forcePosition = false;
    }

    public float GetMouseSpeed()
    {
        return mouseSpeed;
    }
}
