﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RandomExt
{
    public static float GetRandomSign()
    {
        return Random.value > .5f ? 1f : -1f;
    }

    public static Vector2 GetRandomPointOnCircleEdge(float radius)
    {
        Vector2 point = Random.insideUnitCircle.normalized * radius;
        return new Vector3(point.x, point.y, 0f);
    }
}
