﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// List extension methods
/// </summary>
public static class ListExt
{
    /// <summary>
    /// Returns the first element from this list.
    /// </summary>
    public static T First<T>(this List<T> list)
    {
        return list[0];
    }

    /// <summary>
    /// Returns the last element from this list.
    /// </summary>
    public static T Last<T>(this List<T> list)
    {
        return list[list.Count - 1];
    }

    /// <summary>
    /// Returns a random element from this list.
    /// </summary>
    public static T Random<T>(this List<T> list)
    {
        return list[UnityEngine.Random.Range(0, list.Count)];
    }

    /// <summary>
    /// Returns a random element from this list and removes the random element from this list.
    /// </summary>
    public static T GetRandomAndRemoveAt<T>(this List<T> list)
    {
        int randLoc = UnityEngine.Random.Range(0, list.Count);
        T item = list[randLoc];
        list.RemoveAt(randLoc);
        return item;
    }
}
