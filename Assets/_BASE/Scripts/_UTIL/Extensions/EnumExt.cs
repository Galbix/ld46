﻿using System;
using System.Collections;
using System.Collections.Generic;

/// <summary> Enum Extension Methods </summary>
/// <typeparam name="T"> type of Enum </typeparam>
public class EnumExt<T> where T : struct, IConvertible
{
    public static int Count
    {
        get
        {
            if (!typeof(T).IsEnum)
                throw new ArgumentException("T must be an enumerated type");

            return Enum.GetNames (typeof(T)).Length;
        }
    }
        
    public static T GetRandomValue()
    {
        Array v = Enum.GetValues (typeof (T));
        return (T) v.GetValue (new Random ().Next(v.Length));
    }
}