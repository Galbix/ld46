﻿using UnityEngine;

/// <summary>
/// Vector3 and Vector2 extension methods
/// </summary>
public static class VectorExt
{
    #region Vector Equal
    /// <summary>
    /// Checks if a Vector3 is -almost- equal to another
    /// </summary>
    public static bool V3Equal(this Vector3 a, Vector3 b, float equalMargin = .001f)
    {
        return Vector3.SqrMagnitude(a - b) < equalMargin;
    }
    /// <summary>
    /// Checks if a Vector2 is -almost- equal to another
    /// </summary>
    public static bool V2Equal(this Vector2 a, Vector2 b, float equalMargin = .001f)
    {
        return Vector2.SqrMagnitude(a - b) < equalMargin;
    }
    #endregion

    #region Vector SetXYZ
    /// <summary>
    /// Returns this Vector2 with a specified X value.
    /// </summary>
    public static Vector2 WithX(this Vector2 vec, float x)
    {
        vec.x = x;
        return vec;
    }
    /// <summary>
    /// Returns this Vector2 with a specified Y value.
    /// </summary>
    public static Vector2 WithY(this Vector2 vec, float y)
    {
        vec.y = y;
        return vec;
    }

    /// <summary>
    /// Returns this Vector3 with a specified X value.
    /// </summary>
    public static Vector3 WithX(this Vector3 vec, float x)
    {
        vec.x = x;
        return vec;
    }
    /// <summary>
    /// Returns this Vector3 with a specified Y value.
    /// </summary>
    public static Vector3 WithY(this Vector3 vec, float y)
    {
        vec.y = y;
        return vec;
    }
    /// <summary>
    /// Returns this Vector3 with a specified Z value.
    /// </summary>
    public static Vector3 WithZ(this Vector3 vec, float z)
    {
        vec.z = z;
        return vec;
    }

    /// <summary>
    /// Returns this Vector3 with a specified X and Y value.
    /// </summary>
    public static Vector3 WithXY(this Vector3 vec, float x, float y)
    {
        vec.x = x;
        vec.y = y;
        return vec;
    }
    /// <summary>
    /// Returns this Vector3 with a specified X and Z value.
    /// </summary>
    public static Vector3 WithXZ(this Vector3 vec, float x, float z)
    {
        vec.x = x;
        vec.z = z;
        return vec;
    }
    /// <summary>
    /// Returns this Vector3 with a specified Y and Z value.
    /// </summary>
    public static Vector3 WithYZ(this Vector3 vec, float y, float z)
    {
        vec.y = y;
        vec.z = z;
        return vec;
    }
    #endregion

    #region Vector Flip
    /// <summary>
    /// Flips the X, and Y of this Vector2.
    /// </summary>
    public static Vector2 Flip(this Vector2 vec)
    {
        return new Vector2(vec.y, vec.x);
    }
    /// <summary>
    /// Flips the X, Y, and Z of this Vector3.
    /// </summary>
    public static Vector3 Flip(this Vector3 vec)
    {
        return new Vector3(vec.z, vec.y, vec.x);
    }
    #endregion
    
    public static Vector2 GetDirection(this Transform originTransform, Vector2 target)
    {        
        Vector2 heading = target - (Vector2)originTransform.position;
        float distance = heading.magnitude;
        return  heading / distance;
    }

    public static Vector2 GetNormal(this Vector2 vector)
    {
        return new Vector2(vector.y, -vector.x);
    }

    public static Vector2 GetNormalCounterClockwise(this Vector2 vector)
    {
        return new Vector2(-vector.y, vector.x);
    }
}