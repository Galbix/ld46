﻿using UnityEngine;

/// <summary>
/// Color extension methods
/// </summary>
public static class ColorExt
{
    /// <summary>
    /// Returns this Color with a specified R value.
    /// </summary>
    public static Color WithR(this Color c, float r)
    {
        c.r = r;
        return c;
    }

    /// <summary>
    /// Returns this Color with a specified G value.
    /// </summary>
    public static Color WithG(this Color c, float g)
    {
        c.g = g;
        return c;
    }

    /// <summary>
    /// Returns this Color with a specified B value.
    /// </summary>
    public static Color WithB(this Color c, float b)
    {
        c.b = b;
        return c;
    }

    /// <summary>
    /// Returns this Color with a specified A value.
    /// </summary>
    public static Color WithA(this Color c, float a)
    {
        c.a = a;
        return c;
    }

    /// <summary>
    /// Returns this Color with no (0) alpha value
    /// </summary>
    public static Color NoAlpha(this Color c)
    {
        c.a = 0f;
        return c;
    }

    /// <summary>
    /// Returns this Color with full (1) alpha value
    /// </summary>
    public static Color FullAlpha(this Color c)
    {
        c.a = 1f;
        return c;
    }
}
