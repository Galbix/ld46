﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Float and Int extension methods
/// </summary>
public static class MathExt
{
    #region FloatExt
    /// <summary>
    /// Returns this float rounded up or down based on the specified midpoint.
    /// </summary>
    public static float RoundAt(this float f, float midpoint)
    {
        midpoint += Mathf.Floor(f);
        return f < midpoint ? Mathf.Floor(f) : Mathf.Ceil(f);
    }

    /// <summary>
    /// Returns the value of this instance multiplied by the time in seconds it took to complete the last frame.
    /// </summary>
    public static float WithDeltaTime(this float f)
    {
        return f * Time.deltaTime;
    }
    /// <summary>
    /// Returns the value of this instance multiplied by the interval in seconds at which physics are performed.
    /// </summary>
    public static float WithFixedDeltaTime(this float f)
    {
        return f * Time.fixedDeltaTime;
    }
    /// <summary>
    /// Returns the value of this instance multiplied by the timeScale-independent time in seconds it took to complete the last frame.
    /// </summary>
    public static float WithUnscaledDeltaTime(this float f)
    {
        return f * Time.unscaledDeltaTime;
    }

    /// <summary>
    /// Returns the value of this instance multiplied by the value of this instance.
    /// </summary>
    public static float Squared(this float f)
    {
        return f * f;
    }
    #endregion

    #region IntExt
    /// <summary>
    /// Returns a valid index position in a List from a given int
    /// </summary>
    public static int ValidateIndex<T>(this int chosenIndex, List<T> listToValidate)
    {
        if (chosenIndex < 0)
            return listToValidate.Count;

        if (chosenIndex > listToValidate.Count - 1)
            return 0;

        return chosenIndex;
    }

    /// <summary>
    /// Returns the value of this instance multiplied by the value of this instance.
    /// </summary>
    public static int Squared(this int i)
    {
        return i * i;
    }
    #endregion
}
