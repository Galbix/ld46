﻿
/// <summary>
/// String extension methods
/// </summary>
public static class StringExt
{
    /// <summary>
    /// Truncate a string by a maxLength number of characters
    /// </summary>
    public static string Truncate(this string s, int maxLength)
    {
        if (string.IsNullOrEmpty(s)) return s;
        return s.Length <= maxLength ? s : s.Substring(0, maxLength);
    }

    /// <summary>
    /// Returns this string with an uppercase first letter.
    /// </summary>
    public static string UppercaseFirst(this string s)
    {
        if (string.IsNullOrEmpty(s)) return string.Empty;
        return char.ToUpper(s[0]) + s.Substring(1);
    }
}