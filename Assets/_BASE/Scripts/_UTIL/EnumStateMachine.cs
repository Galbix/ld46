﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

// T is enum
// S is EnumState
public class EnumStateMachine<T, S> where T : struct, IComparable where S : EnumState<T>
{
    private EnumState<T> currentState = null;
    private T currentEnum;
    private Dictionary<T, EnumState<T>> states = new Dictionary<T, EnumState<T>>();
        
    // Previous State
    private EnumState<T> previousState = null;
    private T previousEnum;

    // Previous State
    private EnumState<T> nextState = null;
    private T nextEnum;

    #region GET ENUM / STATE
    public T GetCurrentStateEnum()
    {
        return currentEnum;
    }
    
    public EnumState<T> GetCurrentState()
    {
        return currentState;
    }

    public T GetPreviousStateEnum()
    {
        return previousEnum;
    }
    
    public EnumState<T> GetPreviousState()
    {
        return previousState;
    }

    public T GetNextStateEnum()
    {
        return nextEnum;
    }
    
    public EnumState<T> GetNextState()
    {
        return nextState;
    }
    #endregion
    
    public void InitStates(MonoBehaviour baseClass, T initialState, params EnumState<T>[] enumStates)
    {
        for(int i = 0; i < enumStates.Length; i++)
        {
            enumStates[i].Init(baseClass);
            AddState(enumStates[i].GetState(), enumStates[i]);
        }
        
        ChangeState(initialState);
    }
    
    private void AddState(T stateEnum, EnumState<T> stateInstance)
    {
        states.Add(stateEnum, stateInstance);
    }
    
    public void ChangeState(T newEnum)
    {
        if(newEnum.Equals(currentEnum)) return;
        
        EnumState<T> newState = states[newEnum];
        
        previousState = currentState;
        previousEnum = currentEnum;

        nextState = newState;
        nextEnum = newEnum;

        ExitState(currentState, nextEnum);
        currentState = newState;
        currentEnum = newEnum;
        EnterState(currentState, previousEnum);
    }
    
    public void UpdateState()
    {
        if(currentState == null) return;
        
    	currentState.Update();
    }
    
    public void FixedUpdateState()
    {
        if(currentState == null) return;
        
    	currentState.FixedUpdate();
    }
    
    private void ExitState(EnumState<T> state, T _nextState)
    {
        if(state == null) return;
        
    	state.Exit(_nextState);
    }
    
    private void EnterState(EnumState<T> state, T _previousState)
    {
        if(state == null) return;
        
    	state.Enter(_previousState);
    }
}

[System.Serializable]
public abstract class EnumState<E> where E : struct, IComparable
{    
    public abstract void Init(MonoBehaviour baseClass);
        
    public abstract E GetState();
    
    public virtual void Update() { }
    public virtual void FixedUpdate() { }
    
    public virtual void Enter(E previousState) { }
    public virtual void Exit(E nextState) { }
}


// Fastest way of using the EnumStateMachine (via inheritance)
// Inconvenience 1: Can't really be use along with the Singleton parent class
// Inconvenience 2: Only one state machine per class, additional state machines have to use instances
public abstract class EnumStateMachineMonoBehaviour<T, S> : SerializedMonoBehaviour where T : struct, IComparable where S : EnumState<T>
{
    #pragma warning disable 0649
    [ValidateInput("IsValidState"), SerializeField]
    private EnumState<T>[] states;
    [SerializeField]
    private T initialState;
    #pragma warning restore 0649
    
    protected EnumStateMachine<T, S> stateMachine 
        = new EnumStateMachine<T, S>();
    
    protected virtual void Start() 
    {
        stateMachine.InitStates(this, initialState, states);
    }
    
    protected virtual void Update() 
    {
        stateMachine.UpdateState();
    }
    
    protected virtual void FixedUpdate() 
    {
        stateMachine.UpdateState();
    }
    
    // Inspector ValidateInput in 'states' variables
    // Will check for duplicate enum values
    private bool IsValidState(EnumState<T>[] _states)
    {
        List<T> enumStates = new List<T>();
        
        for(int i = 0; i < _states.Length; i++)
        {
            if(enumStates.Contains(_states[i].GetState()))
            {
                return false;
            }
            
            enumStates.Add(_states[i].GetState());
        }
        
        return true;
    }
}