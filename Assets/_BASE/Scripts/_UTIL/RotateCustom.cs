﻿using UnityEngine;
using System.Collections;

public class RotateCustom : MonoBehaviour
{
    public bool worldSpace;
    public Vector3 rotationVector;
    public float rotationSpeed;

    private void Update()
    {
        ExecuteRotation();
    }

    private void ExecuteRotation()
    {
        if(worldSpace)
        {
            transform.Rotate(rotationVector * rotationSpeed, Space.World);
        }
        else
        {
            transform.Rotate(rotationVector * rotationSpeed);
        }
    }
}
