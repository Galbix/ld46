﻿using UnityEngine;

[System.Serializable]
public class TweenCustom
{
    public float tweenTime = 0f;
    public AnimationCurve tweenCurve = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(1f, 1f));
}
