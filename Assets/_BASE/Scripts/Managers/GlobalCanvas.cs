﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class GlobalCanvas : MonoBehaviour
{
    [Header("Time Scale")]
    public TMP_Text timeScaleText;

    private void OnEnable()
    {
        DebugTimeScale.onTimeScaleChange += UpdateTimeScaleText;
    }
    
    private void OnDisable()
    {
        DebugTimeScale.onTimeScaleChange -= UpdateTimeScaleText;
    }

    #region DEBUG TIME SCALE
    public void UpdateTimeScaleText(float timeScale)
    {
        string timeScaleStr = "x" + timeScale.ToString("N1");
        timeScaleText.text = timeScaleStr;
        
        Sequence timeScaleSequence = DOTween.Sequence();
        
        DOTween.Kill("UpdateTimeScale", false);
        
        timeScaleSequence.SetUpdate(true);
        timeScaleSequence.SetId("UpdateTimeScale");
        timeScaleSequence.Append(timeScaleText.DOFade(1f, .5f));
        timeScaleSequence.AppendInterval(1f);
        timeScaleSequence.Append(timeScaleText.DOFade(0f, 2f));
    }
    #endregion
}
