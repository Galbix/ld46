﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MEC;

public class SoundManager : Singleton<SoundManager>
{
    [Header("Audio Sources")]
    public List<AudioSource> sfxChannels;
    public AudioSource musicSource;
    public AudioSource loopSource;

    // small pitch variation for diversity
    [Header("Pitch Variations")]
    public float lowPitchRange = .95f;
    public float highPitchRange = 1.05f;

    public const float KEEP_SAME_VOLUME = -1f;
    
    // SFX Channels
    public enum Channel
    {
        PLANET, // HURT & HEAL
        CITY_BULLET,
        CITY_UPGRADE, // & HOVER
        HAZARDS,
        GAMEOVER,
        LASER,
        PLANET_DRAG,
        CITY_EXP,
        AVAILABLE_UPGRADE,

        Length
    }
    
    
    #region UNITY METHODS
    protected override void Awake() 
    {
        base.Awake();
        
        CheckAudioSources();
    }
    
    private void CheckAudioSources()
    {
        int currentNumChannels = sfxChannels.Count;
        int totalNumChannels = (int)Channel.Length;
        
        if(currentNumChannels > totalNumChannels)
        {
            return;
        }
        
        int channelsNeeded = totalNumChannels - currentNumChannels;
        
        for(int i = 0; i < channelsNeeded; i++)
        {
            AddChannel();
        }
    }
    #endregion

    #region Main Song
    public void FadeInAllSounds(float newMainTime, float targetVolume = KEEP_SAME_VOLUME)
    {
        FadeInMain(newMainTime, targetVolume);
        
        for(int i = 0; i < sfxChannels.Count; i++)
        {
            Timing.RunCoroutine(FadeInVolume(sfxChannels[i], newMainTime, targetVolume));
        }
    }
    
    public void FadeOutAllSounds(float newMainTime)
    {
        FadeOutMain(newMainTime);
        
        for(int i = 0; i < sfxChannels.Count; i++)
        {
            Timing.RunCoroutine(FadeOutVolume(sfxChannels[i], newMainTime));
        }
    }
    
    public void FadeInMain(float newMainTime, float targetVolume)
    {
        Timing.RunCoroutine(FadeInVolume(musicSource, newMainTime, targetVolume));
    }
    
    public void FadeOutMain(float newMainTime)
    {
        Timing.RunCoroutine(FadeOutVolume(musicSource, newMainTime));
    }

    private IEnumerator<float> FadeInVolume(AudioSource audioSource, float _newTime, float targetVolume = KEEP_SAME_VOLUME)
    {
        if(targetVolume == KEEP_SAME_VOLUME)
        {
            targetVolume = audioSource.volume;
        }

        float initialTime = 0f;
        audioSource.volume = 0f;
        
        while (initialTime < _newTime)
        {
            initialTime += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(0f, targetVolume, initialTime / _newTime);
            yield return Timing.WaitForOneFrame;
        }
    }
    
    private IEnumerator<float> FadeOutVolume(AudioSource audioSource, float _newTime)
    {
        float initialTime = 0f;
        float initialVolume = audioSource.volume;

        while (initialTime < _newTime)
        {
            initialTime += Time.deltaTime;
            audioSource.volume = Mathf.Lerp(initialVolume, 0f, initialTime / _newTime);
            yield return Timing.WaitForOneFrame;
        }
    }
    
    public void FadeMain(AudioClip newMainClip, float newMainTime)
    {
        Timing.RunCoroutine(FadeOutVolume(newMainClip, newMainTime));
    }

    private IEnumerator<float> FadeOutVolume(AudioClip _newMain, float _newTime)
    {
        float initialTime = 0f;
        float initialVolume = musicSource.volume;

        while (initialTime < _newTime)
        {
            initialTime += Time.deltaTime;
            musicSource.volume = Mathf.Lerp(initialVolume, 0f, initialTime / _newTime);
            yield return Timing.WaitForOneFrame;
        }
        
        musicSource.clip = _newMain;
        musicSource.Play();

        Timing.RunCoroutine(FadeInVolume(_newTime));
    }

    private IEnumerator<float> FadeInVolume(float _newTime)
    {
        float initialTime = 0f;
        float initialVolume = musicSource.volume;

        while (initialTime < _newTime)
        {
            initialTime += Time.deltaTime;
            musicSource.volume = Mathf.Lerp(initialVolume, 1f, initialTime / _newTime);
            yield return Timing.WaitForOneFrame;
        }
    }
    
    public void ChangeMainSong(AudioClip main)
    {
        musicSource.clip = main;
        musicSource.Play();
    }

    public void ModulateMainPitch(float newPitch)
    {
        musicSource.pitch = newPitch;
    }

    public void StopMain()
    {
        musicSource.Stop();
    }

    public void PlayMain()
    {
        musicSource.Play();
    }
    #endregion
    
    #region SFX Sounds
    public void PlaySFX(Channel channel, bool randomizePitch, bool waitForEnd, AudioClip clip, float volume = KEEP_SAME_VOLUME)
    {
        PlaySFX((int)channel, randomizePitch, waitForEnd, clip, volume);
    }
    
    public void PlaySFX(int channel, bool randomizePitch, bool waitForEnd, AudioClip clip, float volume = KEEP_SAME_VOLUME)
    {
        AudioSource currentAudioSource = GetAudioSourceByIndex(channel);
        currentAudioSource.clip = clip;

        if (waitForEnd && currentAudioSource.isPlaying)
        {
            return;
        }
        
        if(volume != KEEP_SAME_VOLUME)
        {
            currentAudioSource.volume = volume;
        }

        if(randomizePitch)
        {
            currentAudioSource.pitch = GetRandomPitch();
        }

        currentAudioSource.Play();
    }
    
    public void PlayRandomizeSFX(Channel channel, bool randomizePitch, bool waitForEnd, float volume = KEEP_SAME_VOLUME, params AudioClip[] clips)
    {
        PlayRandomizeSFX((int)channel, randomizePitch, waitForEnd, volume, clips);
    }

    public void PlayRandomizeSFX(int channel, bool randomizePitch, bool waitForEnd, float volume = KEEP_SAME_VOLUME, params AudioClip[] clips)
    {
        PlaySFX(channel, randomizePitch, waitForEnd, clips[Random.Range(0, clips.Length)], volume);
    }
    
    private AudioSource GetAudioSourceByIndex(int index)
    {
        if(index <= 0)
        {
            return GetDefaultAudioSource();
        }
        
        return sfxChannels[index];
    }
    
    private AudioSource GetDefaultAudioSource()
    {
        return sfxChannels[0];
    }
    
    private void AddChannel()
    {
        sfxChannels.Add(NewAudioSource());
    }
    
    private AudioSource NewAudioSource()
    {
        AudioSource newAudioSource = this.gameObject.AddComponent<AudioSource>();
        newAudioSource.playOnAwake = false;
        newAudioSource.loop = false;
        newAudioSource.volume = 1f;
        newAudioSource.pitch = 1f;
        return newAudioSource;
    }
    #endregion

    #region SFX Loop
    public void StartLoop(AudioClip clip, float volume)
    {
        loopSource.Stop();
        loopSource.volume = volume;
        loopSource.clip = clip;

        loopSource.Play();
    }

    public void EndLoop()
    {
        loopSource.Stop();
        loopSource.volume = 0f;
    }

    public bool IsLoopActive()
    {
        return loopSource.isPlaying;
    }
    #endregion

    private float GetRandomPitch()
    {
        return Random.Range(lowPitchRange, highPitchRange);
    }
}