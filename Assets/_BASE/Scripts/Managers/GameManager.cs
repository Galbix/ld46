﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GameManager : Singleton<GameManager>
{
	public enum GameState
    {
        NONE,
        INTRO,
        PLAY,
        OVER
    }

    public GameObject soundManagerPrefab;
    
    [Header("Game States")]
    public GameManagerNoneState gameManagerNoneState;
    public GameManagerIntroState gameManagerIntroState;
    public GameManagerPlayState gameManagerPlayState;
    public GameManagerOverState gameManagerOverState;
    
    [Header("Fades - Scene MAIN")]
    public Fade playFadeIn;
    public Fade gameOverFadeOut;

    [Header("Fades - Scene OVER")]
    public Fade gameOverFadeIn;
    public Fade retryFadeOut;

    [Header("SFXs")]
    public AudioClip gameOverSFX;

    private const GameState INITIAL_GAME_STATE = GameState.INTRO;
    
    private EnumStateMachine<GameState, GameManagerBaseState> stateMachineGameManager 
        = new EnumStateMachine<GameState, GameManagerBaseState>();
    
    #region UNITY METHODS
    private void Start() 
    {
        DontDestroyOnLoad(this.gameObject);

        stateMachineGameManager.InitStates(this, INITIAL_GAME_STATE, gameManagerNoneState, 
            gameManagerIntroState, gameManagerPlayState, gameManagerOverState);

        ScoreData.InitScores();
    }
    
    private void Update()
    {
        stateMachineGameManager.UpdateState();

        /*if(Input.GetKeyDown(KeyCode.T))
        {
            Debug.LogFormat(">>> SCREEN RESOLUTION, HEIGHT: {0}, WIDTH: {1}", Screen.height, Screen.width);
            Debug.Log(">>> CAMERA ORTOGRAPHIC SIZE: " + CameraController.Instance.cam.orthographicSize);
            Debug.Log(">>> CAMERA ASPECT RATIO: " + CameraController.Instance.cam.aspect);
        }*/
    }
    
    private void FixedUpdate()
    {
        stateMachineGameManager.FixedUpdateState();
    }
    #endregion

    public void ChangePlay()
    {
        stateMachineGameManager.ChangeState(GameState.PLAY);
    }

    public void ChangeGameOver()
    {
        stateMachineGameManager.ChangeState(GameState.OVER);
    }
    
    public void Play()
    {
        TransitionController.FadeIn(playFadeIn);
    }

    public void FadeInOver()
    {
        TransitionController.FadeIn(gameOverFadeIn);
    }
    
    public void Over()
    {
        CanvasManager.Instance.SaveScore();
        SoundManager.Instance.PlaySFX(SoundManager.Channel.GAMEOVER, false, true, gameOverSFX);
        
        TransitionController.FadeOut(gameOverFadeOut, () => { 
            SceneController.Load(SceneController.SceneName.OVER);
        });
    }

    public void Retry()
    {
        TransitionController.FadeOut(retryFadeOut, () => { 
            SceneController.Load(SceneController.SceneName.MAIN);
        });
    }

    public bool IsPlaying()
    {
        return stateMachineGameManager.GetCurrentStateEnum().Equals(GameState.PLAY);
    }
}

[System.Serializable]
public abstract class GameManagerBaseState : EnumState<GameManager.GameState>
{
    protected GameManager gameManager;
    
    public override void Init(MonoBehaviour baseClass)
    {
        gameManager = (GameManager)baseClass;
    }

    public override void Enter(GameManager.GameState previouState)
    {
        base.Enter(previouState);
        //Debug.Log("Enter State: " + GetState());
    }

    public override void Exit(GameManager.GameState previouState)
    {
        base.Exit(previouState);
        //Debug.Log("Exit State: " + GetState());
    }
}

[System.Serializable]
public class GameManagerNoneState : GameManagerBaseState
{
    public override GameManager.GameState GetState()
    {
        return GameManager.GameState.NONE;
    } 
}

[System.Serializable]
public class GameManagerIntroState : GameManagerBaseState
{
    public override GameManager.GameState GetState()
    {
        return GameManager.GameState.INTRO;
    }
}

[System.Serializable]
public class GameManagerPlayState : GameManagerBaseState
{
    public override GameManager.GameState GetState()
    {
        return GameManager.GameState.PLAY;
    }
    
    public override void Enter(GameManager.GameState previouState)
    {
        base.Enter(previouState);
        gameManager.Play();
    }
}

[System.Serializable]
public class GameManagerOverState : GameManagerBaseState
{
    public override GameManager.GameState GetState()
    {
        return GameManager.GameState.OVER;
    }
    
    public override void Enter(GameManager.GameState previouState)
    {
        base.Enter(previouState);
        gameManager.Over();
    }
}