﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MEC;
using TMPro;
using Sirenix.OdinInspector;

public class CanvasManager : Singleton<CanvasManager>
{
    public ScoreProgression[] scoreProgressions;

    [Header("Fade")]
    public Image fadeImage;

    //[Header("Years")]
    //public TMP_Text yearsTMP;

    [Header("Health")]
    public TMP_Text healthTMP;

    [System.Serializable]
    public struct ScoreProgression
    {
        public ScoreData.Degree degree;
        public float speed;
        public bool noLimit;
        [HideIf("noLimit")]
        public float limit;
        public GameObject parent;
        public TMP_Text text;
    }

    private Dictionary<ScoreData.Degree, ScoreProgression> scoreProgressionsDict = new Dictionary<ScoreData.Degree, ScoreProgression>();

    // Years
    //private const float TIME_YEAR_SPEED = 100000f;
    //private float currentYear = 1;

    private float scoreTime = 1;
    private ScoreData.Degree scoreDegree = ScoreData.Degree.NONE;
    private string degreeString = string.Empty;
    private GameObject scoreParent;
    private TMP_Text scoreText;

    // Health
    private int planetBaseHealth;

    protected override void Awake()
    {
        base.Awake();

        scoreProgressionsDict.Clear();

        for (int i = 0; i < scoreProgressions.Length; i++)
        {
            scoreProgressionsDict.Add(scoreProgressions[i].degree, scoreProgressions[i]);
        }
    }

    private void Update()
    {
        UpdateScore();
    }
    #region FADE 
    public void PreLoadFadeIn(Fade fade)
    {
        fadeImage.color = fade.fadeColor.FullAlpha();
    }

    #region FadeIn
    public CoroutineHandle FadeIn(float fadeTime)
    {
        return LerpUtil.LerpColor(fadeImage, fadeImage.color.NoAlpha(), fadeTime);
    }

    public CoroutineHandle FadeIn(float fadeTime, Color fadeColor)
    {
        fadeImage.color = fadeColor.FullAlpha();
        return LerpUtil.LerpColor(fadeImage, fadeColor.NoAlpha(), fadeTime);
    }

    public CoroutineHandle FadeIn(float fadeTime, AnimationCurve fadeCurve)
    {
        fadeImage.color = fadeImage.color.FullAlpha();
        return LerpUtil.LerpColor(fadeImage, fadeImage.color.NoAlpha(), fadeTime, fadeCurve);
    }

    public CoroutineHandle FadeIn(float fadeTime, AnimationCurve fadeCurve, Color fadeColor)
    {
        fadeImage.color = fadeColor.FullAlpha();
        return LerpUtil.LerpColor(fadeImage, fadeColor.NoAlpha(), fadeTime, fadeCurve);
    }

    public CoroutineHandle FadeIn(Fade fadeInfo)
    {
        fadeImage.color = fadeInfo.fadeColor.FullAlpha();
        return LerpUtil.LerpColor(fadeImage, fadeInfo.fadeColor.NoAlpha(), fadeInfo.tweenTime, fadeInfo.tweenCurve);
    }
    #endregion

    #region FadeOut
    public CoroutineHandle FadeOut(float fadeTime)
    {
        return LerpUtil.LerpColor(fadeImage, fadeImage.color.FullAlpha(), fadeTime);
    }

    public CoroutineHandle FadeOut(float fadeTime, Color fadeColor)
    {
        fadeImage.color = fadeColor.NoAlpha();
        return LerpUtil.LerpColor(fadeImage, fadeColor.FullAlpha(), fadeTime);
    }

    public CoroutineHandle FadeOut(float fadeTime, AnimationCurve fadeCurve)
    {
        return LerpUtil.LerpColor(fadeImage, fadeImage.color.FullAlpha(), fadeTime, fadeCurve);
    }

    public CoroutineHandle FadeOut(float fadeTime, AnimationCurve fadeCurve, Color fadeColor)
    {
        fadeImage.color = fadeColor.NoAlpha();
        return LerpUtil.LerpColor(fadeImage, fadeColor.FullAlpha(), fadeTime, fadeCurve);
    }

    public CoroutineHandle FadeOut(Fade fadeInfo)
    {
        fadeImage.color = fadeInfo.fadeColor.NoAlpha();
        return LerpUtil.LerpColor(fadeImage, fadeInfo.fadeColor.FullAlpha(), fadeInfo.tweenTime, fadeInfo.tweenCurve);
    }
    #endregion

    #endregion
    

    private bool ShouldUpdateScore()
    {
        if(GameManager.Instance == null)
        {
            return false;
        }

        if(!GameManager.Instance.IsPlaying())
        {
            return false;
        }

        return true;
    }

    private void UpdateScore()
    {
        if(!ShouldUpdateScore()) return;

        scoreTime += Time.deltaTime * GetTimeSpeed();

        CheckLimit();
        RefreshScore();
    }

    private void CheckLimit()
    {
        if(scoreProgressionsDict[scoreDegree].noLimit) return;

        if(scoreTime > scoreProgressionsDict[scoreDegree].limit)
        {
            UpgradeDegree();
        }
    }

    public void InitDegree()
    {
        UpgradeDegree();
    }

    private void UpgradeDegree()
    {
        if(scoreParent)
        {
            scoreParent.SetActive(false);
        }

        scoreTime = 1;
        scoreDegree = (ScoreData.Degree)((int)scoreDegree + 1);
        degreeString = Score.GetScoreString(scoreDegree);
        scoreText = scoreProgressionsDict[scoreDegree].text;
        scoreParent = scoreProgressionsDict[scoreDegree].parent;
        
        if(scoreParent)
        {
            scoreParent.SetActive(true);
        }
    }

    private float GetTimeSpeed()
    {
        return scoreProgressionsDict[scoreDegree].speed;
    }

    public void RefreshScore()
    {
        scoreText.text = GetScoreInt().ToString() + degreeString;
    }

    public void SaveScore()
    {
        ScoreData.SaveCurrentScore(GetScoreInt(), scoreDegree);
    }

    private int GetScoreInt()
    {
        return Mathf.RoundToInt(scoreTime);
    }

    public void InitHealth(int initialHealth)
    {
        planetBaseHealth = initialHealth;
        RefrehsHealth(planetBaseHealth);
    }

    public void RefrehsHealth(int currentHealth)
    {
        //Debug.Log("RefrehsHealth: " + currentHealth);
        healthTMP.text = currentHealth + " / " + planetBaseHealth;
    }
}

public static class ScoreData
{
    public enum Degree
    {
        NONE,

        YEARS,
        MILLIONS,
        BILLIONS,
        BAZILLIONS
    }

    public static Score currentScore = null;
    public static Score bestScore = null;

    private static string BEST_SCORE_TIME_KEY = "BEST_SCORE_TIME";
    private static string BEST_SCORE_DEGREE_KEY = "BEST_SCORE_DEGREE";

    public static void InitScores()
    {
        DeleteCurrentScore();
        LoadBestScorePrefs();
    }

    public static void SaveCurrentScore(int time, Degree degree)
    {
        currentScore = new Score(time, degree);

        /*Debug.Log("SaveCurrentScore currentScore time: " + currentScore.time);
        Debug.Log("SaveCurrentScore currentScore degree: " + currentScore.degree);
        Debug.Log("SaveCurrentScore currentScore degree int: " + (int)currentScore.degree);

        if(bestScore != null)
        {
            Debug.Log("SaveCurrentScore best time: " + bestScore.time);
            Debug.Log("SaveCurrentScore best degree: " + bestScore.degree);
            Debug.Log("SaveCurrentScore best degree int: " + (int)bestScore.degree);
        }
        else
        {
            Debug.Log("SaveCurrentScore NO best score");
        }*/

        if(IsCurrentBetterThanBestScore(currentScore))
        {
            SaveBestScorePrefs(currentScore);
        }
    }

    private static bool IsCurrentBetterThanBestScore(Score score)
    {
        if(!HaveBestScore())
        {
            return true;
        }

        return score.IsBetterThan(bestScore);
    }

    public static void DeleteCurrentScore()
    {
        currentScore = null;
    }

    public static void LoadBestScorePrefs()
    {
        //Debug.Log("LoadBestScorePrefs 1");
        if(!HasBestScoreInPrefs()) return;
        //Debug.Log("LoadBestScorePrefs 2");

        int bestTime = PlayerPrefs.GetInt(BEST_SCORE_TIME_KEY, -1);
        Degree bestDegree = (Degree)PlayerPrefs.GetInt(BEST_SCORE_DEGREE_KEY, (int)Degree.NONE);
        
        //Debug.Log("LOAD bestTime: " + bestTime);
        //Debug.Log("LOAD bestDegree (int): " + (int)bestDegree);
        //Debug.Log("LOAD bestDegree: " + bestDegree);

        bestScore = new Score(bestTime, bestDegree);
    }

    private static void SaveBestScorePrefs(Score _bestScore)
    {
        bestScore = _bestScore;
        PlayerPrefs.SetInt(BEST_SCORE_TIME_KEY, bestScore.time);
        PlayerPrefs.SetInt(BEST_SCORE_DEGREE_KEY, (int)bestScore.degree);
        
        //Debug.Log("SAVE bestTime: " + bestScore.time);
        //Debug.Log("SAVE bestDegree (int): " + (int)bestScore.degree);
        //Debug.Log("SAVE bestDegree: " + bestScore.degree);
    }

    private static bool HasBestScoreInPrefs()
    {
        return PlayerPrefs.HasKey(BEST_SCORE_TIME_KEY);
    }

    public static bool HaveBestScore()
    {
        return bestScore != null;
    }

    public static void DeleteAllPlayerPrefs()
    {
        Debug.Log("DeleteAllPlayerPrefs!");
        PlayerPrefs.DeleteAll();
    }
}


public class Score
{
    public int time;
    public ScoreData.Degree degree;

    public Score(int _time, ScoreData.Degree _degree)
    {
        time = _time;
        degree = _degree;
    }

    public bool IsBetterThan(Score score)
    {
        if((int)degree > (int)score.degree)
        {
            return true;
        }

        if((int)degree < (int)score.degree)
        {
            return false;
        }

        return time > score.time;
    }

    public string GetScoreText()
    {
        return time + GetScoreString(degree);
    }

    public static string GetScoreString(ScoreData.Degree degree)
    {
        switch(degree)
        {
            case ScoreData.Degree.YEARS:
            default:
                return " YEARS";

            case ScoreData.Degree.MILLIONS:
                return " MILLION YEARS";

            case ScoreData.Degree.BILLIONS:
                return " BILLION YEARS";

            case ScoreData.Degree.BAZILLIONS:
                return " BAZILLION YEARS";
        }
    }
}