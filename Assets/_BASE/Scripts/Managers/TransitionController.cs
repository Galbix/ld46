﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public static class TransitionController
{
    private static bool isTransitionActive = false;
    
    public static bool IsTransitionActive()
    {
        return isTransitionActive;
    }
    
    public static void FadeIn(Fade fadeIn, Action OnTransitionEnd = null)
    {
        Timing.RunCoroutine(_FadeInTransition(fadeIn, OnTransitionEnd));
    }
    
    private static IEnumerator<float> _FadeInTransition(Fade fadeIn, Action OnTransitionEnd)
    {
        isTransitionActive = true;
        
        if(CanvasManager.Instance)
        {
            CanvasManager.Instance.PreLoadFadeIn(fadeIn);
        }
        
        yield return Timing.WaitForSeconds(fadeIn.preDelayTime);
        
        SoundManager.Instance.FadeInAllSounds(fadeIn.tweenTime);
        
        if(CanvasManager.Instance)
        {
            yield return Timing.WaitUntilDone(CanvasManager.Instance.FadeIn(fadeIn));
        }
        
        yield return Timing.WaitForSeconds(fadeIn.postDelayTime);
        
        isTransitionActive = false;
        
        if(OnTransitionEnd != null)
        {
            OnTransitionEnd();
        }
    }
    
    public static void FadeOut(Fade fadeOut, Action OnTransitionEnd = null)
    {
        Timing.RunCoroutine(_FadeOutTransition(fadeOut, OnTransitionEnd));
    }
    
    private static IEnumerator<float> _FadeOutTransition(Fade fadeOut, Action OnTransitionEnd = null)
    {
        isTransitionActive = true;
        
        SoundManager.Instance.FadeOutAllSounds(fadeOut.tweenTime);
        
        yield return Timing.WaitForSeconds(fadeOut.preDelayTime);
        
        if(CanvasManager.Instance)
        {
            yield return Timing.WaitUntilDone(CanvasManager.Instance.FadeOut(fadeOut));
        }
        
        yield return Timing.WaitForSeconds(fadeOut.postDelayTime);
        
        isTransitionActive = false;
        
        if(OnTransitionEnd != null)
        {
            OnTransitionEnd();
        }
    }
}
