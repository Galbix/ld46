using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using MEC;

public class CameraController : Singleton<CameraController>
{
    [Header("Camera Base")]
    public Transform childCamera;

    [Header("Blit")]
    public Material blitMaterial;
    public Texture[] possibleTextures;
    
    // Shake
    private ShakeInstance shakeInstance = new ShakeInstance();
    private CoroutineHandle shakeCoroutine;

    // COMPONENTS
    [HideInInspector]public Camera cam;
    private Blit blit;

    protected override void Awake()
    {
        base.Awake();
        cam = GetComponentInChildren<Camera>();
        blit = GetComponent<Blit>();
    }

    #region Shader Blit Transitions
    public void StartBlitTransition(float _time, AnimationCurve _ac, bool distort)
    {
        ChooseRandomTexture();

        if (distort)
            blitMaterial.SetFloat("_Distort", 1f);

        Timing.RunCoroutine(TextureLerp(_time, 1f, _ac));
    }
    
    public void StartBlitTransition(TweenCustom tween, bool distort)
    {
        StartBlitTransition(tween.tweenTime, tween.tweenCurve, distort);
    }

    public void EndBlitTransition(float _time, AnimationCurve _ac)
    {
        blitMaterial.SetFloat("_Distort", 0f);
        Timing.RunCoroutine(TextureLerp(_time, 0f, _ac));
    }
    public void EndBlitTransition(TweenCustom tween)
    {
        EndBlitTransition(tween.tweenTime, tween.tweenCurve);
    }

    private void ChooseRandomTexture()
    {
        Texture chosenTexture = possibleTextures[Random.Range(0, possibleTextures.Length)];
        blitMaterial.SetTexture("_Transition", chosenTexture);
    }

    private IEnumerator<float> TextureLerp(float totalTime, float destValue, AnimationCurve ac)
    {
        float initialTime = 0f;
        float initialValue = destValue.Equals(0f) ? 1f : 0f;
        blitMaterial.SetFloat("_Cutoff", initialValue);
        float currentValue = initialValue;

        while (initialTime < totalTime)
        {
            initialTime += Time.deltaTime;
            currentValue = Mathf.Lerp(initialValue, destValue, ac.Evaluate(initialTime / totalTime));
            blitMaterial.SetFloat("_Cutoff", currentValue);
            yield return Timing.WaitForOneFrame;
        }
    }
    #endregion

    #region ScreenShake Effect
    public void ScreenShake(Shake shake)
    {
        shakeInstance.ExecuteTimedShake(childCamera, shake.shakeTime, shake.shakeAmount, shake.shakeDecrease, shake.shakeDir);
    }

    public void ScreenShake(float shakeTime, float shakeAmount, float shakeDecrease, Vector3 shakeDir)
    {
        shakeInstance.ExecuteTimedShake(childCamera, shakeTime, shakeAmount, shakeDecrease, shakeDir);
    }

    public void ScreenShakeActivate(Shake shake)
    {
        if (shakeCoroutine != null)
        {
            shakeInstance.ResumeShakeMovement();
        }
        else
        {
            shakeCoroutine = shakeInstance.ExecuteShakeLoop(childCamera, shake.shakeAmount, shake.shakeDelay, shake.shakeDir);
        }
    }
    
    public void ScreenShakeDectivate()
    {
        shakeInstance.StopShakeMovement();
    }
    #endregion

}
