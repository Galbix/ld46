﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DebugTimeScale : MonoBehaviour
{
    public float mouseScrollSpeed = .1f;
    public float mouseScrollAmount = 1f;
    
    public delegate void OnTimeScaleChange(float timeScale);
    public static event OnTimeScaleChange onTimeScaleChange;
    
    private const float INITIAL_TIME_SCALE = 1f;
    
    private void Update()
    {
        if(CanCheckTimeScaleInput())
        {
            CheckTimeScaleInput();
        }
    }

    private bool CanCheckTimeScaleInput()
    {
#if UNITY_EDITOR

        if(EditorWindow.mouseOverWindow != null &&
            EditorWindow.mouseOverWindow.ToString() != " (UnityEditor.GameView)")
        {
            return false;
        }
#else
        return false;
#endif

        return true;
    }

    private void CheckTimeScaleInput()
    {
        ChangeTimeScaleScroll();

        if(Input.GetKeyDown(KeyCode.Mouse2))
        {
            ResetTimeScale();
        }
    }
    
    private void ChangeTimeScaleScroll()
    {
        float currentTimeScale = Time.timeScale;
        
        float mouseScrollDirection = 0f;
        
        if(Input.mouseScrollDelta.y != 0f)
        {
            mouseScrollDirection = Input.mouseScrollDelta.y > 0f ?
                mouseScrollAmount : mouseScrollAmount * -1f ;
        }
        
        float mouseScrollValue = mouseScrollDirection * mouseScrollSpeed;
        
        currentTimeScale += mouseScrollValue;
        
        if(!currentTimeScale.Equals(Time.timeScale))
        {
            UpdateTimeScale(currentTimeScale);
        }
    }
    
    private void ResetTimeScale()
    {
        UpdateTimeScale(INITIAL_TIME_SCALE);
    }
    
    private void UpdateTimeScale(float newTimeScale)
    {
        if(newTimeScale < 0f)
        {
            newTimeScale = 0f;
        }
        
        Time.timeScale = newTimeScale;
        
        if(onTimeScaleChange != null)
        {
            onTimeScaleChange(Time.timeScale);
        }
    }
}
