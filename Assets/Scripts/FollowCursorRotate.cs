﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCursorRotate : MonoBehaviour
{
    public Transform targetTransform;

    private void Update()
    {
        targetTransform.rotation = QuaternionUtil.GetFacingDirection2D(CursorCustom.Instance.GetWorldPosition());
    }
}
