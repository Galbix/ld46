﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagneticField : MonoBehaviour
{
    private List<BaseElement> capturedElements = new List<BaseElement>();

    private const float ORBIT_SPEED = .1f;
    private const float MAX_SPEED_CAPTURE = .5f;

    /*private void FixedUpdate()
    {
        Vector3 target = Planet.Instance.GetCenter();

        for (int i = capturedElements.Count - 1; i >= 0 ; i--)
        {
            if(capturedElements[i] == null)
            {
                capturedElements.Remove(capturedElements[i]);
                continue;
            }

            Quaternion q = Quaternion.AngleAxis (ORBIT_SPEED, transform.forward);
            capturedElements[i].rb2d.MovePosition (q * (capturedElements[i].rb2d.transform.position - target) + target);
            capturedElements[i].rb2d.MoveRotation (capturedElements[i].rb2d.transform.rotation * q); 
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        BaseElement element = other.GetComponent<BaseElement>();

        if(element && CanCaptureElement(element))
        {
            ApplyMagneticForce(element);
        }
    }

    private bool CanCaptureElement(BaseElement element)
    {
        Debug.LogFormat("Element {0} with rb speed {1}", element.gameObject.name, element.GetCurrentSpeedRB());

        if(element.GetCurrentSpeedRB() > MAX_SPEED_CAPTURE)
        {
            return false;
        }

        return true;
    }

    private void ApplyMagneticForce(BaseElement element)
    {
        capturedElements.Add(element);
    }*/
}
