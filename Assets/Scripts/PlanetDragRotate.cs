﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlanetDragRotate : MonoBehaviour, IPointerEnterHandler,
    IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    public Transform minRadiusTransform;
    public Transform maxRadiusTransform;
    public Sprite nonTutoSprite;

    [Header("SFXs")]
    public AudioClip hoverSFX;
    public AudioClip dragSFX;
    
    private float currentAngle = 0f;
    private float previousAngle = 0f;
    private float currentRadius = 0f;
    private PlanetData.AngleDirection angleDirection = PlanetData.AngleDirection.NONE;
    private float moveAngleHold = 0f;
    private float initialHoldRotZ;
    private float minRadius, maxRadius;

    // Controls
    private bool isPointerInside = false;
    private bool isPointerDown = false;
    private bool isMouseDown = false;
    private bool draggingStarted = false;

    // COMPONENTS
    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        minRadius = Vector2.Distance(minRadiusTransform.position, Planet.Instance.GetCenter());
        maxRadius = Vector2.Distance(maxRadiusTransform.position, Planet.Instance.GetCenter());
    }
    
    private void Update()
    {
        spriteRenderer.enabled = ShouldShowVisualAid();
        isMouseDown = Input.GetMouseButton(0);

        if(PointersData.BLOCK_PLANET_DOWN_EVENTS)
        {
            isMouseDown = false;
        }

        CheckDrag();
    }
    
    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        isPointerInside = true;
        SoundManager.Instance.PlaySFX(SoundManager.Channel.PLANET_DRAG, true, false, hoverSFX);
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        isPointerInside = false;
    }

    public void OnPointerDown(PointerEventData pointerEventData)
    {
        isPointerDown = true;
        moveAngleHold = 0f;
        initialHoldRotZ = Planet.Instance.parentTransform.rotation.eulerAngles.z;
        currentRadius = Vector2.Distance(CursorCustom.Instance.GetWorldPosition(), Planet.Instance.GetCenter());

        SoundManager.Instance.PlaySFX(SoundManager.Channel.PLANET_DRAG, true, false, dragSFX);
    }

    public void OnPointerUp(PointerEventData pointerEventData)
    {
        isPointerDown = false;
        CursorCustom.Instance.EndForcePosition();

        spriteRenderer.sprite = nonTutoSprite;
    }

    private void CheckDrag()
    {
        Vector2 currentCursorPos = CursorCustom.Instance.GetWorldPosition();
        Vector2 planetCenterPos = Planet.Instance.GetCenter();

        currentAngle = (Mathf.Atan2(currentCursorPos.y -planetCenterPos.y, planetCenterPos.x - currentCursorPos.x) * 180f / Mathf.PI) + 180f;
        angleDirection = GetAngleDirection(previousAngle, currentAngle);

        currentRadius = Vector2.Distance(currentCursorPos, planetCenterPos);

        if(IsDragging())
        {
            draggingStarted = true;
            moveAngleHold += (previousAngle - currentAngle);
            Planet.Instance.parentTransform.rotation = Quaternion.Euler(Vector3.forward * (initialHoldRotZ + moveAngleHold));
            Planet.Instance.ChangeAngleDirection(angleDirection);
        }
        else
        {
            draggingStarted = false;
        }

        previousAngle = currentAngle;
    }

    public bool IsDragging()
    {
        if(!isMouseDown)
        {
            return false;
        }

        if(!draggingStarted && !IsRadiusValid())
        {
            return false;
        }
        
        return true;
    }

    private bool ShouldShowVisualAid()
    {
        if(isPointerInside)
        {
            return true;
        }

        if(IsDragging())
        {
            return true;
        }

        return false;
        //return isPointerInside && IsRadiusValid();
    }

    private bool IsPointerHoldingDown()
    {
        return isPointerInside && isPointerDown;
    }

    private bool IsRadiusValid()
    {
        return currentRadius > minRadius && currentRadius < maxRadius;
    }

    private PlanetData.AngleDirection GetAngleDirection(float previousAngle, float currentAngle)
    {
        if(Mathf.Approximately(previousAngle, currentAngle))
        {
            return PlanetData.AngleDirection.NONE;
        }

        float clockWise = 0f;
        float counterClockWise = 0f;

        if (previousAngle <= currentAngle)
        {
            clockWise = currentAngle - previousAngle;
            counterClockWise = previousAngle + (360f - currentAngle);
        }
        else
        {
            clockWise = (360f - previousAngle) + currentAngle;
            counterClockWise = previousAngle - currentAngle;
        }

        if(clockWise <= counterClockWise)
        {
            return PlanetData.AngleDirection.CLOCKWISE;
        }
        else
        {
            return PlanetData.AngleDirection.COUNTER_CLOCKWISE;
        }
    }

}
