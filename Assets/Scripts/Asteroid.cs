﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;

public class Asteroid : BaseElement
{
    [Title("Asteroid")]
    public float speed;
    public float hurtScaleValue;

    private const float INITIAL_RANDOM_TORQUE = 10f;

    // COMPONENTS
    private SpriteRenderer spriteRenderer;

    protected override void Awake()
    {
        base.Awake();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    private void Start()
    {
        moveDirection = VectorUtil.GetDirection(Planet.Instance.GetCenter(), rb2d.position);
        rb2d.velocity = moveDirection * speed;
        rb2d.AddTorque(Random.Range(-INITIAL_RANDOM_TORQUE, INITIAL_RANDOM_TORQUE));
    }

    public override void DamageVisuals()
    {
        spriteRenderer.transform.DOPunchScale(Vector3.one * hurtScaleValue, .1f, 10, 1f);
    }

    protected override void Kill()
    {
        base.Kill();
        spriteRenderer.DOFade(0f, .2f).SetEase(Ease.OutQuint)
            .OnComplete(() => { Destroy(this.gameObject); });
    }
}
