﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[RequireComponent(typeof(Rigidbody2D))]
public abstract class BaseElement : MonoBehaviour
{
    public enum Type
    {
        NONE,

        ASTEROID,
        SPACE_JUNK,
        BIG_ASTEROID,
        METEOR,
        INVADER,

        Length
    }

    public float life;
    public int planetDamage;

    [Header("Experience")]
    public bool spawnExperienceOnDeath = true;
    [ShowIf("spawnExperienceOnDeath")]
    public GameObject experiencePrefab;
    [ShowIf("spawnExperienceOnDeath")]
    public int experienceNumber;
    [ShowIf("spawnExperienceOnDeath")]
    public float expSpawnSpeed;
    
    [Header("New elements")]
    public bool spawnNewElements = false;
    [ShowIf("spawnNewElements")]
    public BaseElement.Type spawnElementType;
    [ShowIf("spawnNewElements")]
    public int spawnElementNum;
    [ShowIf("spawnNewElements")]
    public float spawnPosRange;

    [Header("SFXs")]
    public AudioClip[] destroySFX;

    [HideInInspector]public float distanceToCenter;
    protected Vector2 moveDirection;

    private bool isDeath = false;

    // COMPONENTS
    [HideInInspector]public Rigidbody2D rb2d;
    
    protected virtual void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    protected virtual void Update()
    {
        distanceToCenter = Vector2.Distance(this.transform.position, Planet.Instance.GetCenter());
    }

    public virtual void Damage(float damageAmount)
    {
        if(!CanBeKill()) return;

        life -= damageAmount;
        DamageVisuals();

        if(life <= 0)
        {
            Kill();
        }
    }

    public virtual void DamageVisuals() { }

    protected bool CanBeKill()
    {
        if(isDeath)
        {
            return false;
        }

        return true;
    }

    protected virtual void Kill()
    {
        if(!CanBeKill()) return;

        isDeath = true;
        SpawnExperience();
        SpawnNewElements();

        SoundManager.Instance.PlayRandomizeSFX(SoundManager.Channel.HAZARDS, true, false, SoundManager.KEEP_SAME_VOLUME, destroySFX);
    }

    public Vector2 GetMoveDirection()
    {
        return moveDirection;
    }

    private bool collideWithPlanet = false;
    public void Collision(GameObject other)
    {
        collideWithPlanet = other.CompareTag(Tags.Planet);
        Kill();
    }

    private static float EXP_ANGLE_RANGE = 20f;
    private static float EXP_SPEED_RANGE = .25f;

    protected bool ShouldSpawnExperience()
    {
        if(!spawnExperienceOnDeath)
        {
            return false;
        }

        return true;
    }
    
    protected void SpawnExperience()
    {
        if(!ShouldSpawnExperience()) return;

        float anglePerExp = (360f / experienceNumber);

        for (int i = 0; i < experienceNumber; i++)
        {
            float expAngle = anglePerExp + Random.Range(-EXP_ANGLE_RANGE, EXP_ANGLE_RANGE);
            Vector2 expDirection = Quaternion.AngleAxis(expAngle * i, Vector3.forward) * Vector2.up;
            float expSpeed = expSpawnSpeed + Random.Range(-EXP_SPEED_RANGE, EXP_SPEED_RANGE);
            Vector2 expForce = expDirection * expSpeed;

            Experience experience = (Instantiate(experiencePrefab, this.transform.position, Quaternion.identity) as GameObject).GetComponent<Experience>();
            experience.Spawn(expForce);
        }
    }
    
    protected bool ShouldSpawnNewElement()
    {
        if(!spawnNewElements)
        {
            return false;
        }

        if(collideWithPlanet)
        {
            return false;
        }

        return true;
    }
    
    protected void SpawnNewElements()
    {
        if(!ShouldSpawnNewElement()) return;

        Vector2 spawnAxis = this.transform.GetDirection(Planet.Instance.GetCenter()).GetNormal().normalized;

        Vector2 initSpawnPos = (Vector2)this.transform.position + (spawnAxis * spawnPosRange);
        Vector2 finalSpawnPos = (Vector2)this.transform.position + (spawnAxis * spawnPosRange * -1f);

        for (int i = 0; i < spawnElementNum; i++)
        {
            Vector2 currentSpawnPos = Vector2.Lerp(initSpawnPos, finalSpawnPos, Mathf.InverseLerp(0, spawnElementNum - 1, i));
            LevelManager.Instance.Spawn(spawnElementType, currentSpawnPos);
        }
    }

    public float GetCurrentSpeedRB()
    {
        return rb2d.velocity.magnitude;
    }

    public bool IsAlive()
    {
        return !isDeath;
    }
}
