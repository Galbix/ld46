﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CanvasEnd : MonoBehaviour
{
    public TMP_Text currentScoreTMP;

    public GameObject hiscoreParent;
    public TMP_Text hiscoreTMP;

    private void Start()
    {
        GameManager.Instance.FadeInOver();

        // CURRENT SCORE
        currentScoreTMP.text = ScoreData.currentScore.GetScoreText();

        // HISCORE
        hiscoreParent.SetActive(ScoreData.HaveBestScore());

        if(ScoreData.HaveBestScore())
        {
            hiscoreTMP.text = "HISCORE: " + ScoreData.bestScore.GetScoreText();
        }
    }

    public void PressRetryButton()
    {
        GameManager.Instance.Retry();
    }
}
