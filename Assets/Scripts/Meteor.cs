﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;

public class Meteor : BaseElement
{
    [Title("Meteor")]
    public float speed;
    public float hurtScaleValue;

    private float initialSpriteScale;
    private float initialWidthMult;

    // COMPONENTS
    private SpriteRenderer spriteRenderer;
    private TrailRenderer trailRenderer;

    protected override void Awake()
    {
        base.Awake();
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        trailRenderer = GetComponentInChildren<TrailRenderer>();
    }

    private void Start()
    {
        moveDirection = VectorUtil.GetDirection(Planet.Instance.GetCenter(), rb2d.position);
        rb2d.velocity = moveDirection * speed;

        initialSpriteScale = spriteRenderer.transform.localScale.x;
        initialWidthMult = trailRenderer.widthMultiplier;
    }

    protected override void Update()
    {
        base.Update();

        spriteRenderer.transform.rotation = QuaternionUtil.GetFacingDirection2D(moveDirection);
        trailRenderer.widthMultiplier = (spriteRenderer.transform.localScale.x / initialSpriteScale) * initialWidthMult;
    }

    public override void DamageVisuals()
    {
        spriteRenderer.transform.DOPunchScale(Vector3.one * hurtScaleValue, .1f, 10, 1f);
    }

    protected override void Kill()
    {
        base.Kill();
        Destroy(this.gameObject);
    }
}
