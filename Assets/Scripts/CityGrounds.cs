﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class CityGrounds : MonoBehaviour
{
    public enum Upgrade
    {
        NONE,
        GENERIC,
        SHOT,
        HEAL
    }

    [System.Serializable]
    public struct UpgradeData
    {
        public Upgrade upgrade;
        public GameObject prefab;
        public float xPos;
    }

    [System.Serializable]
    public struct CityLevel
    {
        public UpgradeData[] upgradesData;

        private Dictionary<Upgrade, UpgradeData> upgradesDict;

        public void InitDict()
        {
            upgradesDict = new Dictionary<Upgrade, UpgradeData>();

            for (int i = 0; i < upgradesData.Length; i++)
            {
                upgradesDict.Add(upgradesData[i].upgrade, upgradesData[i]);   
            }
        }

        public GameObject GetPrefab(Upgrade upgrade)
        {
            return upgradesDict[upgrade].prefab;
        }
    }

    public CityLevel[] cityLevels;

    public GameObject availableButtonPrefab;

    [Header("SFXs")]
    public AudioClip upgradeSFX;

    private List<AvailableButton> currentButtons = new List<AvailableButton>();

    private int cityLvl = -1;
    private City currentCity;

    private void Start()
    {
        for (int i = 0; i < cityLevels.Length; i++)
        {
            cityLevels[i].InitDict();
        }
    }

    public void TurnAvailable()
    {
        IncreaseLevel();

        Planet.Instance.RemoveEmptyCity(this);

        for (int i = 0; i < cityLevels[cityLvl].upgradesData.Length; i++)
        {
            Upgrade upgradeType = cityLevels[cityLvl].upgradesData[i].upgrade;

            if(currentCity && !currentCity.IsTypeCompatible(upgradeType))
            {
                continue;
            }

            GameObject availableButtonGO = Instantiate(availableButtonPrefab);
            availableButtonGO.transform.SetParent(this.transform, false);

            AvailableButton availableButton = availableButtonGO.GetComponent<AvailableButton>();
            currentButtons.Add(availableButton);

            availableButton.Show(this, upgradeType,cityLevels[cityLvl].upgradesData[i].xPos);
        }
    }

    public void BuildCity(Upgrade upgradeType)
    {
        if(currentButtons.Count > 0)
        {
            for (int i = 0; i < currentButtons.Count; i++)
            {
                currentButtons[i].Hide();
            }

            currentButtons.Clear();
        }

        if(currentCity && currentCity.gameObject)
        {
            Destroy(currentCity.gameObject);
        }

        GameObject currentCityGO = Instantiate(GetCityPrefab(upgradeType));
        currentCityGO.transform.SetParent(this.transform, false);

        currentCity = currentCityGO.GetComponent<City>();
        currentCity.Init(this);

        SoundManager.Instance.PlaySFX(SoundManager.Channel.CITY_UPGRADE, true, false, upgradeSFX);
    }

    private GameObject GetCityPrefab(Upgrade upgradeType)
    {
        return cityLevels[cityLvl].GetPrefab(upgradeType);
    }

    private void IncreaseLevel()
    {
        cityLvl = cityLvl < cityLevels.Length - 1 ? cityLvl + 1 : cityLvl;
    }
}
