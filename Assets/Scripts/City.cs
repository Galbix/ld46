﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;

public class City : MonoBehaviour
{
    public Transform parenTransform;
    public CityGrounds.Upgrade cityType;

    [Header("Find Targets")]
    public Transform borderLeft, borderRight;
    public LayerMask hazardsLayerMask;
    public LayerMask clearShotLayerMask;

    [Header("Shoot Targets")]
    public bool canShoot = true;
    [ShowIf("canShoot")]
    public Transform[] shotOrigins;
    [ShowIf("canShoot")]
    public GameObject bulletPrefab;
    [ShowIf("canShoot")]
    public int fireRate; // bullets/second
    [ShowIf("canShoot")]
    public float bulletDamage;
    [ShowIf("canShoot")]
    public float bulletSpeed;

    [Header("Heal Planet")]
    public bool shouldHeal = false;
    [ShowIf("shouldHeal")]
    public int healAmount;

    [Header("Attractor")]
    public float attractRadius;
    public LayerMask expLayerMask;
    public float attractSmoothTime;
    public Transform circleDebugTransform;

    [Header("Laser")]
    public LaserGun laserGun;

    [Header("Upgrade")]
    public bool haveUpgrade = true;
    [ShowIf("haveUpgrade")]
    public int expToUpgrade;

    private BaseElement currentTargetElement = null;

    private const int DETECT_HAZARDS_RAY_NUM = 6;
    private const float DETECT_HAZARDS_DISTANCE = 6.5f; //8f
    private const bool DEBUG_DETECT_RAYS = false;

    private Vector2 initialDir;
    private Vector2 finalDir;
    private float angleDiff;
    private float anglePerRay;

    private float bulletTimer = 0f;
    private float bulletDelayTime;

    private int currentExpPoints = 0;

    private const float SPAWN_HUMAN_TIME = 15f;

    private CityGrounds cityGrounds;

    public void Init(CityGrounds _cityGrounds)
    {
        cityGrounds = _cityGrounds;

        parenTransform.DOPunchScale(Vector3.one * .25f, .2f, 10, 1f);

        if(shouldHeal)
        {
            ExecuteHeal();
        }
    }

    private void ExecuteHeal()
    {
        Planet.Instance.Heal(healAmount);
    }

    private void Start()
    {
        initialDir = VectorUtil.GetDirection(borderLeft.position, Planet.Instance.GetCenter());
        finalDir = VectorUtil.GetDirection(borderRight.position, Planet.Instance.GetCenter());
        angleDiff = Vector2.Angle(initialDir, finalDir);
        anglePerRay = (angleDiff / DETECT_HAZARDS_RAY_NUM) * -1f;

        bulletDelayTime = 1f / (float)fireRate;

        DOVirtual.DelayedCall(SPAWN_HUMAN_TIME, CreateHuman, false).SetLoops(-1);

        if(circleDebugTransform)
        {
            circleDebugTransform.localScale = Vector3.one * attractRadius;
        }
    }

    private void CreateHuman()
    {
        if(this == null || this.gameObject == null) return;

        Planet.Instance.SpawnHuman(this.transform.rotation.eulerAngles.z);
    }

    private void Update()
    {
        if(ShouldAttractExperience())
        {
            AttractExperience();
        }

        if(ShouldFindTargets())
        {
            FindTargets();
        }

        if(CanShootTargets())
        {
            ShootTargets();
        }
    }

    private bool ShouldFindTargets()
    {
        if(!canShoot)
        {
            return false;
        }

        return true;
    }

    private void FindTargets()
    {
        initialDir = VectorUtil.GetDirection(borderLeft.position, Planet.Instance.GetCenter());
        finalDir = VectorUtil.GetDirection(borderRight.position, Planet.Instance.GetCenter());

        List<BaseElement> allTargetElements = new List<BaseElement>();

        for (int i = 0; i <= DETECT_HAZARDS_RAY_NUM; i++)
        {
            Vector2 rayDir = Quaternion.AngleAxis(anglePerRay * i, Vector3.forward) * initialDir;
            RaycastHit2D hit = Physics2D.Raycast(Planet.Instance.GetCenter(), rayDir, DETECT_HAZARDS_DISTANCE, hazardsLayerMask);
            Debug.DrawRay(Planet.Instance.GetCenter(), rayDir * DETECT_HAZARDS_DISTANCE, Color.red);

            if(hit.collider != null)
            {
                BaseElement baseElement = hit.collider.GetComponent<BaseElement>();

                if(!allTargetElements.Contains(baseElement))
                {
                    allTargetElements.Add(baseElement);
                }
            }
        }

        for (int i = 0; i < allTargetElements.Count; i++)
        {
            if(currentTargetElement == null || currentTargetElement.distanceToCenter > allTargetElements[i].distanceToCenter)
            {
                currentTargetElement = allTargetElements[i];
            }
        }
    }

    private bool CanShootTargets()
    {
        if(!canShoot)
        {
            return false;
        }

        if(currentTargetElement == null)
        {
            return false;
        }

        if(!HaveClearShotLine())
        {
            return false;
        }

        return true;
    }

    private bool HaveClearShotLine()
    {
        Vector2 rayDir = this.transform.GetDirection(currentTargetElement.transform.position);
        RaycastHit2D hit = Physics2D.Raycast(this.transform.position, rayDir, DETECT_HAZARDS_DISTANCE, clearShotLayerMask);
        Debug.DrawRay(this.transform.position, rayDir * DETECT_HAZARDS_DISTANCE, Color.yellow);

        if(hit.collider == null)
        {
            return false;
        }

        BaseElement hitElement = hit.collider.gameObject.GetComponent<BaseElement>();

        return hitElement == currentTargetElement;
    }

    private void ShootTargets()
    {
        if(DEBUG_DETECT_RAYS)
        {
            Vector2 targetDir = VectorUtil.GetDirection(currentTargetElement.transform.position, Planet.Instance.GetCenter());
            float targetDis = Vector2.Distance(currentTargetElement.transform.position, Planet.Instance.GetCenter());
            Debug.DrawRay(Planet.Instance.GetCenter(), targetDir * targetDis, Color.blue);
        }

        bulletTimer += Time.deltaTime;

        if(bulletTimer > bulletDelayTime)
        {
            bulletTimer = 0f;
            CityBullet bullet = (Instantiate(bulletPrefab, GetRandomShotOrigin(), Quaternion.identity) as GameObject).GetComponent<CityBullet>();
            bullet.Shot(currentTargetElement, bulletDamage, bulletSpeed);
        }
        
        if(laserGun)
        {
            laserGun.AssignTarget(currentTargetElement);
        }
    }

    private Vector2 GetRandomShotOrigin()
    {
        return shotOrigins[Random.Range(0, shotOrigins.Length)].position;
    }

    private bool ShouldAttractExperience()
    {
        if(!haveUpgrade)
        {
            return false;
        }

        return true;
    }

    private void AttractExperience()
    {
        Collider2D[] attractables = Physics2D.OverlapCircleAll(this.transform.position, attractRadius, expLayerMask);

        for (int i = 0; i < attractables.Length; i++)
        {
            Experience experience = attractables[i].GetComponent<Experience>();
            experience.Attract(this, this.transform.position, attractSmoothTime, attractRadius);
        }
    }

    private bool CanAddExpPoints()
    {
        if(!haveUpgrade)
        {
            return false;
        }

        if(currentExpPoints > expToUpgrade)
        {
            return false;
        }

        return true;
    }

    public void AddExpPoints(int expPoints)
    {
        if(!CanAddExpPoints()) return;

        currentExpPoints += expPoints;

        if(currentExpPoints > expToUpgrade)
        {
            ReachUpgrade();
        }
    }

    [Button]
    private void ReachUpgrade()
    {
        cityGrounds.TurnAvailable();
    }

    public bool IsTypeCompatible(CityGrounds.Upgrade upgradeType)
    {
        switch(cityType)
        {
            case CityGrounds.Upgrade.GENERIC:
                return true;
        }

        return cityType == upgradeType;
    }
}
