﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityBullet : MonoBehaviour
{
    [Header("SFXs")]
    public AudioClip bulletSFX;

    private BaseElement targetElement;
    private float damageAmount;
    private float speed;

    private Vector2 targetDirection;

    private const float MIN_DISTANCE_HIT = .1f;
    private const float MAX_LIFETIME = 10f;

    // COMPONENTS
    private Rigidbody2D rb2d;

    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        Destroy(this.gameObject, MAX_LIFETIME);
    }

    private void Update()
    {
        MoveTowardsTarget();
    }

    private void MoveTowardsTarget()
    {
        if(targetElement)
        {
            targetDirection = this.transform.GetDirection(targetElement.transform.position);
        }

        this.transform.Translate(targetDirection * speed * Time.deltaTime);
        CheckDistanceTowardsTarget();
    }

    private void CheckDistanceTowardsTarget()
    {
        if(targetElement == null) return;

        float distanceToTarget = Vector2.Distance(targetElement.transform.position, this.transform.position);

        if(distanceToTarget < MIN_DISTANCE_HIT)
        {
            HitTarget();
        }
    }

    private void HitTarget()
    {
        targetElement.Damage(damageAmount);
        DestroyBullet();
    }

    public void Shot(BaseElement _targetElement, float _damage, float _speed)
    {
        SoundManager.Instance.PlaySFX(SoundManager.Channel.CITY_BULLET, true, false, bulletSFX, .1f);
        
        targetElement = _targetElement;
        damageAmount = _damage;
        speed = _speed;
    }

    private void DestroyBullet()
    {
        Destroy(this.gameObject);
    }
}
