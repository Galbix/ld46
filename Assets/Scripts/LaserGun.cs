﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserGun : MonoBehaviour
{
    public Transform parentHead;

    [Header("SFXs")]
    public AudioClip laserSFX;

    private float laserTimer = 0f;
    private bool laserReady = false;

    private BaseElement currentTargetElement = null;
    private const float LASER_SAME_TARGET_TIME = 1f;

    private const float LASER_DELAY_TIME = 10f;
    
    // COMPONENTS
    private LineRenderer lineRenderer;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        if(!laserReady)
        {
            RefreshLaser();
        }

        if(CanRefreshVisuals())
        {
            RefreshVisuals();
        }

        lineRenderer.enabled = CanRefreshVisuals();
    }

    private void RefreshLaser()
    {
        laserTimer += Time.deltaTime;

        if(laserTimer > LASER_DELAY_TIME)
        {
            LaserReady();
        }
    }

    private void LaserReady()
    {
        laserTimer = 0f;
        laserReady = true;
    }

    private bool CanRefreshVisuals()
    {
        return currentTargetElement != null && currentTargetElement.IsAlive();
    }

    private void RefreshVisuals()
    {
        // head direction
        parentHead.rotation = QuaternionUtil.GetFacingDirection2D(currentTargetElement.transform.position);

        Vector3[] linePositions = new Vector3[]
        {
            this.transform.position,
            currentTargetElement.transform.position
        };
        
        lineRenderer.SetPositions(linePositions);
    }

    public void AssignTarget(BaseElement elementTarget)
    {
        // new target, needs to wait for LASER_SAME_TARGET_TIME
        if(currentTargetElement == null || currentTargetElement != elementTarget)
        {
            laserTimer = LASER_DELAY_TIME - LASER_SAME_TARGET_TIME;
            laserReady = false;
        }

        currentTargetElement = elementTarget;

        if(laserReady)
        {
            LaserShoot(elementTarget);
        }
    }

    private void LaserShoot(BaseElement elementTarget)
    {
        laserReady = false;
        elementTarget.Damage(1000); //insta-kill
        currentTargetElement = null;

        SoundManager.Instance.PlaySFX(SoundManager.Channel.LASER, true, false, laserSFX);
    }
}
