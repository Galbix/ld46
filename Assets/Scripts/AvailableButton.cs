﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using MEC;

public class AvailableButton : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
    [System.Serializable]
    public struct ButtonVisual
    {
        public CityGrounds.Upgrade upgradeType;
        public Sprite sprite;
        public Color color;
    }

    public ButtonVisual[] buttonVisuals;
    public Transform parentTransform;

    [Header("SFXs")]
    public AudioClip hoverSFX;
    public AudioClip showSFX;

    private CityGrounds cityGround;
    private bool isHovering = false;
    private CityGrounds.Upgrade upgradeType;

    private Dictionary<CityGrounds.Upgrade, ButtonVisual> buttonVisualsDict = new Dictionary<CityGrounds.Upgrade, ButtonVisual>();

    private const float Y_LOCAL_POS = 1.95f;

    // COMPONENTS
    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    private void Start()
    {
        InitDict();
    }

    private void InitDict()
    {
        if(buttonVisualsDict.Count > 0) return;

        for (int i = 0; i < buttonVisuals.Length; i++)
        {
            //Debug.LogFormat("Add to dict >> Upgrade Type {0}, Visuals {1}", buttonVisuals[i].upgradeType, buttonVisuals[i]);
            buttonVisualsDict.Add(buttonVisuals[i].upgradeType, buttonVisuals[i]);
        }
    }

    public void OnPointerDown(PointerEventData pointerEventData)
    {
        cityGround.BuildCity(upgradeType);
    }

    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        PointersData.BLOCK_PLANET_DOWN_EVENTS = true;
        isHovering = true;
        parentTransform.DOScale(Vector3.one * 1.25f, .1f).SetEase(Ease.OutCirc);
        SoundManager.Instance.PlaySFX(SoundManager.Channel.CITY_UPGRADE, false, false, hoverSFX);
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        PointersData.BLOCK_PLANET_DOWN_EVENTS = false;
        isHovering = false;
        parentTransform.DOScale(Vector3.one, .1f).SetEase(Ease.InCirc);
    }

    public void Show(CityGrounds _cityGrounds, CityGrounds.Upgrade _upgradeType, float _localPosX)
    {
        InitDict();

        cityGround = _cityGrounds;
        upgradeType = _upgradeType;
        this.transform.localPosition = this.transform.localPosition.WithX(_localPosX);
        spriteRenderer.sprite = buttonVisualsDict[upgradeType].sprite;
        spriteRenderer.color = buttonVisualsDict[upgradeType].color;

        this.gameObject.SetActive(true);
        SoundManager.Instance.PlaySFX(SoundManager.Channel.AVAILABLE_UPGRADE, true, false, showSFX, .5f);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
        Timing.RunCoroutine(_Destroy());
    }

    private IEnumerator<float> _Destroy()
    {
        yield return Timing.WaitForSeconds(.1f);
        PointersData.BLOCK_PLANET_DOWN_EVENTS = false;
        Destroy(this.gameObject);
    }
}

public static class PointersData
{
    public static bool BLOCK_PLANET_DOWN_EVENTS = false;
}
