﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
    public Element[] elements;
    public Transform elementParent;

    private Dictionary<BaseElement.Type, Element> elementsDict = new Dictionary<BaseElement.Type, Element>();
    
    private float timer;

    private const float SPAWN_POINT_RADIUS = 15f;
    public const float MAX_DIFFICULTY_TIME = 360f;

    private void Start()
    {
        for (int i = 0; i < elements.Length; i++)
        {
            elementsDict.Add(elements[i].type, elements[i]);
        }
    }

    private void Update()
    {
        timer += Time.deltaTime;

        for (int i = 0; i < elements.Length; i++)
        {
            Element element = elements[i];

            if(element.CanSpawn(Time.deltaTime, timer))
            {
                if(element.RollSpawn(timer))
                {
                    Spawn(element.type);
                }
            }
        }
    }

    private BaseElement SpawnRandom()
    {
        return Spawn(GetRandomElementPrefab(), GetRandomSpawnPoint());
    }

    public BaseElement Spawn(BaseElement.Type elementType)
    {
        return Spawn(elementsDict[elementType].prefab, GetRandomSpawnPoint());
    }

    public BaseElement Spawn(BaseElement.Type elementType, Vector2 spawnPos)
    {
        return Spawn(elementsDict[elementType].prefab, spawnPos);
    }

    public BaseElement Spawn(GameObject elementPrefab, Vector2 spawnPos)
    {
        GameObject elementGO = Instantiate(elementPrefab, spawnPos, Quaternion.identity) as GameObject;
        elementGO.transform.SetParent(elementParent);

        return (elementGO).GetComponent<BaseElement>();
    }

    private GameObject GetRandomElementPrefab()
    {
        return elements[Random.Range(0, elements.Length)].prefab;
    }

    private Vector2 GetRandomSpawnPoint()
    {
        return Planet.Instance.GetCenter() + RandomExt.GetRandomPointOnCircleEdge(SPAWN_POINT_RADIUS);
    }
}

[System.Serializable]
public class Element
{
    public BaseElement.Type type;
    public GameObject prefab;
    public AnimationCurve chanceCurve;
    public float startInterval;
    public float endInterval;

    private float currentInterval = 0f;
    private float timer = 0f;
    private float currentChance;

    public bool CanSpawn(float deltaTime, float globalTimer)
    {
        timer += deltaTime;
        currentInterval = Mathf.Lerp(startInterval, endInterval, Mathf.InverseLerp(0f, LevelManager.MAX_DIFFICULTY_TIME, globalTimer));
        currentChance = chanceCurve.Evaluate(Mathf.InverseLerp(0f, LevelManager.MAX_DIFFICULTY_TIME, globalTimer));

        if(timer > currentInterval)
        {
            timer = 0f;
            return true;
        }

        return false;
    }

    public bool RollSpawn(float globalTimer)
    {
        //float currentChance = chanceCurve.Evaluate(Mathf.InverseLerp(0f, MAX_DIFFICULTY_TIME, globalTimer));
        return Random.value < currentChance;
    }
}