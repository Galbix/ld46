﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class RotateAroundPlanet : MonoBehaviour
{
    public float speed;
    public bool useRandomDirection;
    [HideIf("useRandomDirection")]
    public PlanetData.AngleDirection direction;

    private const float NO_CUSTOM_SPEED = -1f;

    private void Start()
    {
        if(useRandomDirection)
        {
            direction = PlanetData.GetRandomAngleDirection();
        }
    }

    private void Update()
    {
        ExecuteRotation();
    }

    private void ExecuteRotation()
    {
        transform.Rotate(Vector3.forward * (speed * direction.GetDirectionValue()) * Time.deltaTime);
    }
}
