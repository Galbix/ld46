﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(Rigidbody2D))]
public class Planet : Singleton<Planet>
{
    public Transform parentTransform;
    public float rotationSpeed;
    public PlanetDragRotate planetDragRotate;
    public int initialHealth;
    public CityGrounds[] allCityGrounds;

    [Header("Humans")]
    public Transform humansParent;
    public GameObject humanPrefab;
    
    [Header("Hurt")]
    public AnimationCurve hurtEffectCurve;
    public Shake hurtCameraShake;

    [Header("SFXs")]
    public AudioClip hurtSFX;
    public AudioClip healSFX;

    private Vector3 pointDownPos;
    private Vector3 currentCursorPos;

    // Direction
    private PlanetData.AngleDirection currentAngleDir = PlanetData.AngleDirection.COUNTER_CLOCKWISE;

    // Cities
    private const float FIRST_CITY_TIME = 2f;
    private const float SPAWN_CITY_TIME = 30f; // 60f
    private List<CityGrounds> emptyCityGrounds = new List<CityGrounds>();

    // Health
    private int currentHealth;
    private bool isGettingDamaged = false;

    // COMPONENTS
    private Rigidbody2D rb2d;

    protected override void Awake()
    {
        base.Awake();
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        GameManager.Instance.ChangePlay();
        CanvasManager.Instance.InitDegree();

        currentHealth = initialHealth;
        CanvasManager.Instance.InitHealth(initialHealth);

        emptyCityGrounds.AddRange(allCityGrounds);

        DOVirtual.DelayedCall(FIRST_CITY_TIME, () => {
            TurnCityAvailable();
            DOVirtual.DelayedCall(SPAWN_CITY_TIME, TurnCityAvailable, false).SetLoops(-1);
        });
    }

    private void Update()
    {
    #if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.R))
        {
            ScoreData.DeleteAllPlayerPrefs();
        }

        if(Input.GetKeyDown(KeyCode.K))
        {
            Damage(1, Vector2.up);
        }
    #endif

        if(ShouldExecuteNormalRotation())
        {
            ExecuteNormalRotation();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        BaseElement baseElement = other.gameObject.GetComponent<BaseElement>();

        if(baseElement && baseElement.IsAlive())
        {
            Damage(baseElement.planetDamage, baseElement.GetMoveDirection());
            baseElement.Collision(this.gameObject);
        }
    }

    private bool ShouldExecuteNormalRotation()
    {
        if(planetDragRotate.IsDragging())
        {
            return false;
        }

        return true;
    }

    private void ExecuteNormalRotation()
    {
        parentTransform.Rotate(Vector3.forward * rotationSpeed * currentAngleDir.GetDirectionValue());
    }

    public void ChangeAngleDirection(PlanetData.AngleDirection newAngleDirection)
    {
        if(newAngleDirection.Equals(PlanetData.AngleDirection.NONE)) return;
        
        currentAngleDir = newAngleDirection;
    }

    public Vector2 GetCenter()
    {
        return parentTransform.position;
    }

    private void TurnCityAvailable()
    {
        if(this == null || this.gameObject == null) return;
        if(!HaveEmptyCitiesLeft()) return;

        CityGrounds chosenCityGrounds = GetRandomEmptyCity();
        chosenCityGrounds.TurnAvailable();
    }

    private bool HaveEmptyCitiesLeft()
    {
        return emptyCityGrounds.Count > 0f;
    }

    private CityGrounds GetRandomEmptyCity()
    {
        return emptyCityGrounds[Random.Range(0, emptyCityGrounds.Count)];
    }

    public void RemoveEmptyCity(CityGrounds cityGrounds)
    {
        if(emptyCityGrounds.Contains(cityGrounds))
        {
            emptyCityGrounds.Remove(cityGrounds);
        }
    }

    public void SpawnHuman(float rotZ)
    {
        GameObject humanGO = Instantiate(humanPrefab, humansParent.position, Quaternion.Euler(Vector3.forward * rotZ));
        humanGO.transform.SetParent(humansParent);
    }

    public void Heal(int healAmount)
    {
        currentHealth += healAmount;

        if(healAmount > 0)
        {
            HealVisuals();
        }

        if(currentHealth > initialHealth)
        {
            currentHealth = initialHealth;
        }

        CanvasManager.Instance.RefrehsHealth(currentHealth);
    }

    private void HealVisuals()
    {
        SoundManager.Instance.PlaySFX(SoundManager.Channel.PLANET, true, false, healSFX);
        parentTransform.DOPunchScale(Vector3.one * .05f, .8f, 10, 1f);
    }

    private bool CanDamage()
    {
        if(isGettingDamaged)
        {
            return false;
        }
        
        return true;
    }


    public void Damage(int damageAmount, Vector2 damageDirection)
    {
        if(!CanDamage()) return;

        //Debug.LogFormat("--- DAMAGE!");
        //Debug.LogFormat("OLD currentHealth: " + currentHealth);
        //Debug.LogFormat("damageAmount: " + damageAmount);
        currentHealth -= damageAmount;
        //Debug.LogFormat("NEW currentHealth: " + currentHealth);

        if(damageAmount > 0)
        {
            DamageVisuals(damageDirection);
        }

        if(currentHealth <= 0)
        {
            currentHealth = 0;
            GameOver();
        }

        CanvasManager.Instance.RefrehsHealth(currentHealth);
    }

    private void DamageVisuals(Vector2 damageDirection)
    {
        SoundManager.Instance.PlaySFX(SoundManager.Channel.PLANET, true, false, hurtSFX);
        isGettingDamaged = true;

        parentTransform.DOPunchScale(Vector3.one * -.1f, .1f, 10, 1f).OnComplete(EndDamage);

        Vector2 punchPos = (Vector2) parentTransform.localPosition + (damageDirection * .2f);
        parentTransform.DOPunchPosition(punchPos, .3f, 10, 1f).SetEase(hurtEffectCurve);

        CameraController.Instance.ScreenShake(hurtCameraShake);
    }

    private void EndDamage()
    {
        isGettingDamaged = false;
    }

    private void GameOver()
    {
        Debug.Log(">>> GAME OVER!!");
        GameManager.Instance.ChangeGameOver();
    }
}

public static class PlanetData
{
    public enum AngleDirection
    {
        NONE,
        CLOCKWISE,
        COUNTER_CLOCKWISE
    }

    private static float[] ANGLEDIRECTION_VALUES = new float[]
    {
        0f,
        -1f,
        1f
    };

    public static float GetDirectionValue(this AngleDirection angleDirection)
    {
        return ANGLEDIRECTION_VALUES[(int)angleDirection];
    }

    public static AngleDirection GetRandomAngleDirection()
    {
        return Random.value > .5f ? AngleDirection.CLOCKWISE : AngleDirection.COUNTER_CLOCKWISE;
    }
}