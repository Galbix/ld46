﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Experience : MonoBehaviour
{
    public int expPoints;
    public Mode attractionMode = Mode.DISTANCE;

    private bool IsModeTime()
    {
        return attractionMode.Equals(Mode.TIME);
    }

    public float minMoveSpeed;
    public float maxMoveSpeed;

    public AnimationCurve attractionCurve;

    [ShowIf("IsModeTime")]
    public float timeForMaxSpeed;

    public float moveToCenteSpeed;

    public enum Mode
    {
        DISTANCE,
        TIME
    }

    [Header("SFXs")]
    public AudioClip expSFX;

    private const float INITIAL_RANDOM_TORQUE = 5f;

    // Private variables
    private bool canBeAttracted = false;
    private bool isBeingAttracted = false;
    private bool reachTarget = false;

    private float attractDelayTimer = 0f;
    private const float ATTRACTED_DELAY_TIME = 1f;

    private const float MIN_DISTANCE_REACH = .25f;

    private Transform currentAttractor = null;

    // Mode: TIME
    private float attractTimer = 0f;

    // COMPONENTS
    private Rigidbody2D rb2d;

    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if(ShouldCheckAtrractDelay())
        {
            CheckAtrractDelay();
        }
        
        if(ShouldMoveToCenter())
        {
            MoveToCenter();
        }

        if(isBeingAttracted)
        {
            attractTimer += Time.deltaTime;
        }
    }

    public void Spawn(Vector2 moveForce)
    {
        rb2d.AddForce(moveForce, ForceMode2D.Impulse);
        rb2d.AddTorque(Random.Range(-INITIAL_RANDOM_TORQUE, INITIAL_RANDOM_TORQUE));
    }

    private bool CanBeAttracted()
    {
        return canBeAttracted;
    }
    
    public void Attract(City city, Vector2 targetPos, float smoothTime, float farDistance)
    {
        if(!CanBeAttracted()) return;

        if(city.transform != currentAttractor)
        {
            if(!IsAttractorCloser(city.transform))
            {
                return;
            }
        }

        currentAttractor = city.transform;
        isBeingAttracted = true;

        Vector2 heading = targetPos - GetPos();
        float distance = heading.magnitude;
        Vector2 direction = heading / distance;

        rb2d.velocity = direction * GetMoveSpeed(distance, farDistance);

        if(distance < MIN_DISTANCE_REACH)
        {
            ReachCity(city);
        }
    }

    private bool IsAttractorCloser(Transform newAttractor)
    {
        float newAttractorDistance = Vector2.Distance(newAttractor.position, this.transform.position);
        return newAttractorDistance < GetDistanceToCurrentAttractor();
    }

    private float GetDistanceToCurrentAttractor()
    {
        if(currentAttractor)
        {
            return Vector2.Distance(currentAttractor.position, this.transform.position);
        }

        return Mathf.Infinity;
    }

    private void ReachCity(City city)
    {
        canBeAttracted = false;
        isBeingAttracted = false;
        reachTarget = true;

        city.AddExpPoints(expPoints);
        SoundManager.Instance.PlaySFX(SoundManager.Channel.CITY_EXP, true, false, expSFX, .1f);

        DestroyExperience();
    }

    private void DestroyExperience()
    {
        Destroy(this.gameObject);
    }

    private float GetMoveSpeed(float currentDistance, float farDistance)
    {
        switch(attractionMode)
        {
            case Mode.DISTANCE:
            default:
                return GetDistanceMoveSpeed(currentDistance, farDistance);

            case Mode.TIME:
                return GetTimeMoveSpeed();
        }
    }
    
    private float GetDistanceMoveSpeed(float currentDistance, float farDistance)
    {
        return Mathf.Lerp(minMoveSpeed, maxMoveSpeed, attractionCurve.Evaluate(Mathf.InverseLerp(farDistance, 0f, currentDistance)));
    }

    private float GetTimeMoveSpeed()
    {
        return Mathf.Lerp(minMoveSpeed, maxMoveSpeed, attractionCurve.Evaluate(Mathf.InverseLerp(0f, timeForMaxSpeed, attractTimer)));
    }

    private Vector2 GetPos()
    {
        return this.transform.position;
    }

    private bool ShouldCheckAtrractDelay()
    {
        if(canBeAttracted)
        {
            return false;
        }

        if(reachTarget)
        {
            return false;
        }

        return true;
    }

    private void CheckAtrractDelay()
    {
        attractDelayTimer += Time.deltaTime;

        if(attractDelayTimer > ATTRACTED_DELAY_TIME)
        {
            attractDelayTimer = 0f;
            canBeAttracted = true;
        }
    }

    private bool ShouldMoveToCenter()
    {
        if(!canBeAttracted)
        {
            return false;
        }

        if(isBeingAttracted)
        {
            return false;
        }

        if(reachTarget)
        {
            return false;
        }

        return true;
    }

    private void MoveToCenter()
    {
        Vector2 centerDir = this.transform.GetDirection(Planet.Instance.GetCenter());
        rb2d.velocity = centerDir * moveToCenteSpeed;
    }
}
