﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Shield : MonoBehaviour
{
    public Transform parentTransform;
    public int initialHealth = 2;
    public Sprite damagedBarrierSprite;

    private int currentHealth;
    private bool isGettingDamaged = false;

    // COMP
    private SpriteRenderer spriteRenderer;

    private void Awake()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
    }

    private void Start()
    {
        currentHealth = initialHealth;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        BaseElement baseElement = other.gameObject.GetComponent<BaseElement>();

        if(baseElement)
        {
            Damage(baseElement.planetDamage);
            baseElement.Collision(this.gameObject);
        }
    }

    private bool CanDamage()
    {
        if(isGettingDamaged)
        {
            return false;
        }

        return true;
    }

    private void Damage(int damageAmount)
    {
        if(!CanDamage()) return;
        
        currentHealth -= damageAmount;

        if(damageAmount > 0)
        {
            DamageVisuals();
        }

        if(currentHealth <= 0)
        {
            currentHealth = 0;
            DestroyBarrier();
        }

        CanvasManager.Instance.RefrehsHealth(currentHealth);
    }

    private void DamageVisuals()
    {
        isGettingDamaged = true;

        Vector2 targetPunchPos = parentTransform.localPosition + (Vector3.down * .2f);
        parentTransform.DOPunchPosition(targetPunchPos, .2f, 10, 1f).OnComplete(EndDamage);

        if(currentHealth < initialHealth / 2)
        {
            spriteRenderer.sprite = damagedBarrierSprite;
        }
    }

    private void EndDamage()
    {
        isGettingDamaged = false;
    }

    private void DestroyBarrier()
    {
        spriteRenderer.DOFade(0f, .2f).SetEase(Ease.OutQuint)
            .OnComplete(() => { Destroy(this.gameObject); });
    }
}
