﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetVisuals : MonoBehaviour
{
    [Header("Colors")]
    public SpriteRenderer[] targetSprites;
    public Color[] possibleColors;

    [Header("Decals")]
    public Transform decalsTransform;
    public float decalsMinX, decalsMinY;
    public float decalsMaxX, decalsMaxY;

    [Header("Eyes")]
    public Transform[] eyesTransform;
    public float eyesMinSize, eyesMaxSize;

    private void Start()
    {
        Color chosenColor = GetRandomColor();
        for (int i = 0; i < targetSprites.Length; i++)
        {
            targetSprites[i].color = chosenColor.WithA(targetSprites[i].color.a);
        }

        decalsTransform.localPosition = GetRandomDecalsPos();

        Vector3 eyeScale = GetRandomEyesSize() * Vector3.one;
        for (int i = 0; i < eyesTransform.Length; i++)
        {
            eyesTransform[i].localScale = eyeScale;
        }
    }

    private Color GetRandomColor()
    {
        return possibleColors[Random.Range(0, possibleColors.Length)];
    }

    private Vector2 GetRandomDecalsPos()
    {
        float randomX = Random.Range(decalsMinX, decalsMaxX);
        float randomY = Random.Range(decalsMinY, decalsMaxY);
        return new Vector2(randomX, randomY);
    }

    private float GetRandomEyesSize()
    {
        return Random.Range(eyesMinSize, eyesMaxSize);
    }
}
