﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewExampleScriptableObject", menuName = "ExampleScriptableObject", order = 1)]
public class ExampleScriptableObject : ScriptableObject
{
    
}
