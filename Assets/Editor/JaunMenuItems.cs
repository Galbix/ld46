﻿using UnityEngine;
using UnityEditor;

public class JaunMenuItems
{
    [MenuItem("Tools/Create Basic Folders/2D")]
    static void CreateBasicFolders2D()
    {
        TryCreateFolder("_Scenes");
        TryCreateFolder("Animations");
        TryCreateFolder("Prefabs");
        TryCreateFolder("ScriptableObjects");
        TryCreateFolder("Scripts");
        TryCreateFolder("Sounds");
        TryCreateFolder("Sprites");
    }

    [MenuItem("Tools/Create Basic Folders/3D")]
    static void CreateBasicFolders3D()
    {
        TryCreateFolder("_Scenes");
        TryCreateFolder("Animations");
        TryCreateFolder("Materials");
        TryCreateFolder("Prefabs");
        TryCreateFolder("ScriptableObjects");
        TryCreateFolder("Scripts");
        TryCreateFolder("Sounds");
        TryCreateFolder("Textures");
    }

    [MenuItem("Tools/Create Basic Folders/All")]
    static void CreateBasicFoldersAll()
    {
        TryCreateFolder("_Scenes");
        TryCreateFolder("Animations");
        TryCreateFolder("Materials");
        TryCreateFolder("Prefabs");
        TryCreateFolder("ScriptableObjects");
        TryCreateFolder("Scripts");
        TryCreateFolder("Sounds");
        TryCreateFolder("Sprites");
        TryCreateFolder("Textures");
    }

    // Check if the folder already exists before creating it
    static void TryCreateFolder(string folderName)
    {
        string folderAbsolutePath = "Assets/" + folderName;

        if(!AssetDatabase.IsValidFolder(folderAbsolutePath))
            AssetDatabase.CreateFolder("Assets", folderName);
        else
            Debug.Log("The folder " + folderName + " already exists");
    }
}
